from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class CourseRating(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    stars = models.FloatField(
        validators=[MinValueValidator(0), MaxValueValidator(5)])
    comment = models.TextField(blank=True)
    owner = models.ForeignKey('auth.User', related_name='course_ratings',
                              on_delete=models.CASCADE)
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    cfu = models.CharField(max_length=8)
    academic_year = models.CharField(max_length=20)

    class Meta:
        ordering = ('created',)
        db_table = 'app_course_rating'


class InstructorRating(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    stars = models.FloatField(
        validators=[MinValueValidator(0), MaxValueValidator(5)])
    comment = models.TextField(blank=True)
    owner = models.ForeignKey('auth.User', related_name='instructor_ratings',
                              on_delete=models.CASCADE)
    instructor = models.ForeignKey('Instructor', on_delete=models.CASCADE)

    class Meta:
        ordering = ('created',)
        db_table = 'app_instructor_rating'


class Cdl(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        ordering = ('name',)


class Instructor(models.Model):
    name = models.CharField(max_length=50)
    stars = models.FloatField(
        validators=[MinValueValidator(0), MaxValueValidator(5)], default=0)

    class Meta:
        ordering = ('name',)


class Course(models.Model):
    instructors = models.ManyToManyField('Instructor')
    academic_year = models.CharField(max_length=20)
    cfu = models.IntegerField()
    cdl = models.ForeignKey(
        'Cdl', related_name='courses', on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    code = models.CharField(max_length=20)
    department = models.CharField(max_length=50)
    sector = models.CharField(max_length=100)
    teaching_schedule = models.CharField(max_length=50)
    teaching_period = models.CharField(max_length=50)
    mutuazioni = models.TextField()
    content = models.TextField()
    textbooks = models.TextField()
    objectives = models.TextField()
    methods = models.TextField()
    exam = models.TextField()
    program = models.TextField()
    link = models.CharField(max_length=100)
    stars = models.FloatField(
        validators=[MinValueValidator(0), MaxValueValidator(5)], default=0)

    class Meta:
        ordering = ('title',)


class Pds(models.Model):
    name = models.CharField(max_length=100)
    courses = models.ManyToManyField('Course')
    owner = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    cdl = models.ForeignKey('Cdl', on_delete=models.CASCADE)
    last_update = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-last_update',)
