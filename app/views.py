from datetime import datetime

from django.contrib.auth.models import User
from rest_framework import generics
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework.reverse import reverse

from app.models import (Cdl, Course, CourseRating, Instructor,
                        InstructorRating, Pds)
from app.permissions import IsOwnerOrReadOnly, isOwnerOrDenied
from app.serializers import (CdlDetailSerializer, CdlListSerializer,
                             CourseDetailSerializer, CourseListSerializer,
                             CourseRatingSerializer,
                             InstructorRatingSerializer, InstructorSerializer,
                             PdsSerializer, UserSerializer)


class ApiRoot(generics.GenericAPIView):
    name = 'api-root'

    def get(self, request, *args, **kwargs):
        return Response({
            'cdls': reverse(CdlList.name, request=request),
            'pds': reverse(PdsList.name, request=request),
            'courses': reverse(CourseList.name, request=request),
            'instructors': reverse(InstructorList.name, request=request),
            'course-ratings': reverse(CourseRatingsList.name,
                                      request=request),
            'instructor-ratings': reverse(InstructorRatingsList.name,
                                          request=request)
        })


class CdlList(generics.ListAPIView):
    queryset = Cdl.objects.all()
    serializer_class = CdlListSerializer
    name = 'cdl-list'


class CdlDetail(generics.RetrieveAPIView):
    queryset = Cdl.objects.all()
    serializer_class = CdlDetailSerializer
    name = 'cdl-detail'


class CourseListOld(generics.ListAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseListSerializer
    name = 'course-list'


class CourseList(generics.ListAPIView):
    serializer_class = CourseListSerializer
    name = 'course-list'

    def get_queryset(self):

        # Try to read self.request.query_params.
        # If there are no params, set params to None
        params = getattr(self.request, 'query_params', None)

        # No query params: return all ratings
        if not params:
            return Course.objects.all()

        try:
            cdl_id = params['cdl']
            cdl_id = int(cdl_id)
        except(KeyError, ValueError):
            cdl_id = None
            raise ParseError("Accepted query params:  cdl: int")

        # Valid query param course: return only this course's ratings
        return Course.objects.filter(cdl=cdl_id)


class CourseDetail(generics.RetrieveAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseDetailSerializer
    name = 'course-detail'


class CdlCourseDetail(generics.RetrieveAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseListSerializer
    name = 'cdl-course-detail'

    def get_serializer_context(self):
        return{'request': self.request}


class InstructorDetail(generics.RetrieveAPIView):
    queryset = Instructor.objects.all()
    serializer_class = InstructorSerializer
    name = 'instructor-detail'

    def get_serializer_context(self):
        return{'request': self.request}


class InstructorList(generics.ListAPIView):
    # queryset = Instructor.objects.all()
    serializer_class = InstructorSerializer
    name = 'instructor-list'

    def get_queryset(self):

        # Try to read self.request.query_params.
        # If there are no params, set params to None
        params = getattr(self.request, 'query_params', None)

        # No query params: return all ratings
        if not params:
            return Instructor.objects.all()

        try:
            cdl_id = params['cdl']
            cdl_id = int(cdl_id)
        except(KeyError, ValueError):
            cdl_id = None
            raise ParseError("Accepted query params:  cdl: int")

        # Valid query param
        return Instructor.objects.filter(course__cdl_id=cdl_id)

class CourseRatingsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CourseRating.objects.all()
    serializer_class = CourseRatingSerializer
    name = 'course-rating-detail'
    permission_classes = (IsOwnerOrReadOnly,)

    # Overridden to take care of new stars: they must be saved in Course model.
    def update(self, request, *args, **kwargs):
        try:
            new_stars = request.data.get('stars')
            new_stars = float(
                new_stars) if new_stars is not None else new_stars
        except ValueError:
            raise ParseError("Stars must be of type float")

        rating = self.get_object()
        if new_stars and rating.stars != new_stars:
            course = Course.objects.get(id=rating.course.id)
            count = CourseRating.objects.filter(course=course.id).count()
            updated_stars = ((count * course.stars)
                             + new_stars - rating.stars) / count
            course.stars = updated_stars
            course.save()
        return super(CourseRatingsDetail, self).update(
            request, *args, **kwargs)

    def perform_destroy(self, instance):
        course = Course.objects.get(id=instance.course.id)
        count = CourseRating.objects.filter(course=course.id).count()
        print("Count before destroy:", count)
        if count == 1:
            updated_stars = 0
        else:
            updated_stars = ((count * course.stars)
                             - instance.stars) / (count - 1)
        course.stars = updated_stars
        print("Destroying. New stars:", updated_stars)
        course.save()
        super(CourseRatingsDetail, self).perform_destroy(instance)


class CourseRatingsList(generics.ListCreateAPIView):
    serializer_class = CourseRatingSerializer
    name = 'course-ratings-list'
    # permission_classes = (IsOwnerOrReadOnly)

    def get_queryset(self):
        # Try to read self.request.query_params.
        # If there are no params, set params to None
        params = getattr(self.request, 'query_params', None)

        # No query params: return all ratings
        if not params:
            return CourseRating.objects.all()

        # try:
        #     course_id = int(params['course'])
        # except (KeyError, ValueError):
        #     course_id = None
        #     raise ParseError("Accepted query params:  course: int")

        if 'course' in params:
            course_id = int(params['course'])
            return CourseRating.objects.filter(course=course_id)
        elif 'owner' in params:
            # owner_id = int(params['owner'])
            return CourseRating.objects.filter(owner=self.request.user)
        else:
            raise ParseError(
                "Accepted query params:  course: int | owner: int")

    def perform_create(self, serializer):
        params = getattr(self.request, 'query_params', None)
        try:
            course_id = int(params['course'])
        except (KeyError, ValueError):
            course_id = None
            raise ParseError("Accepted query params:  course: int")

        course = Course.objects.get(id=course_id)
        count = CourseRating.objects.filter(course=course_id).count()

        try:
            new_stars = self.request.data.get('stars')
            new_stars = float(
                new_stars) if new_stars is not None else new_stars
        except ValueError:
            raise ParseError("Stars must be of type float")

        updated_stars = ((count * course.stars) + new_stars) / (count + 1)
        course.stars = updated_stars if count > 0 else new_stars
        course.save()

        serializer.save(course=course,
                        owner=self.request.user)


class InstructorRatingsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = InstructorRating.objects.all()
    serializer_class = InstructorRatingSerializer
    name = 'instructor-rating-detail'
    # permission_classes = (IsOwnerOrReadOnly)

    def update(self, request, *args, **kwargs):
        try:
            new_stars = request.data.get('stars')
            new_stars = float(
                new_stars) if new_stars is not None else new_stars
        except ValueError:
            raise ParseError("Stars must be of type float")

        # Here we have a valid float or None.
        rating = self.get_object()  # get this rating object
        if new_stars and rating.stars != new_stars:
            # Ok, new_stars is not None and we have to update the value since
            # it is different from the last one.
            # print("new stars: ", new_stars)
            instructor = Instructor.objects.get(id=rating.instructor.id)
            count = InstructorRating.objects.filter(
                instructor=instructor.id).count()
            # print("count: ", count)
            updated_stars = ((count * instructor.stars) +
                             new_stars - rating.stars) / count
            instructor.stars = updated_stars
            instructor.save()  # save the updated instructor instance in db

        # Call super.update to actually update the rating in every case.
        return super(InstructorRatingsDetail, self).update(
            request, *args, **kwargs)

    def perform_destroy(self, instance):
        instructor = Instructor.objects.get(id=instance.instructor.id)
        count = InstructorRating.objects.filter(
            instructor=instructor.id).count()
        print("Count before destroy:", count)
        if count == 1:
            updated_stars = 0
        else:
            updated_stars = ((count * instructor.stars)
                             - instance.stars) / (count - 1)
        instructor.stars = updated_stars
        print("Destroying. New stars:", updated_stars)
        instructor.save()
        super(InstructorRatingsDetail, self).perform_destroy(instance)


class InstructorRatingsList(generics.ListCreateAPIView):
    serializer_class = InstructorRatingSerializer
    name = 'instructor-ratings-list'
    # permission_classes = (IsOwnerOrReadOnly)

    def get_queryset(self):
        # Try to read self.request.query_params.
        # If there are no params, set params to None
        params = getattr(self.request, 'query_params', None)

        # No query params: return all ratings
        if not params:
            return InstructorRating.objects.all()

        if 'instructor' in params:
            instructor_id = int(params['instructor'])
            return InstructorRating.objects.filter(instructor=instructor_id)
        elif 'owner' in params:
            # owner_id = int(params['owner'])
            return InstructorRating.objects.filter(owner=self.request.user)
        else:
            raise ParseError(
                "Accepted query params:  instructor: int | owner")

    def perform_create(self, serializer):
        params = getattr(self.request, 'query_params', None)
        try:
            instructor_id = int(params['instructor'])
        except (KeyError, ValueError):
            instructor_id = None
            raise ParseError("Accepted query params:  instructor: int")

        instructor = Instructor.objects.get(id=instructor_id)
        count = InstructorRating.objects.filter(
            instructor=instructor_id).count()
        # print("count:", count)
        try:
            new_stars = self.request.data.get('stars')
            new_stars = float(
                new_stars) if new_stars is not None else new_stars
        except (ValueError):
            raise ParseError("Stars must be of type float")
        # print("new_stars:", new_stars)
        updated_stars = ((count * instructor.stars) + new_stars) / (count + 1)
        # print("updated_stars:", updated_stars)
        # Instructor.objects.filter(id=instructor_id).update(stars=updated_stars)
        instructor.stars = updated_stars if count > 0 else new_stars
        instructor.save()  # necessary to save new stars in instructor table
        # anche se in realtà il campo in InstructorRatingSerializer è una ForeignKey, non un serializer...
        serializer.save(
            instructor=instructor,
            owner=self.request.user
        )


class UserDetail(generics.ListAPIView):
    serializer_class = UserSerializer
    name = 'user-detail'

    def get_queryset(self):
        return User.objects.filter(id=self.request.user.id)


class PdsList(generics.ListCreateAPIView):
    serializer_class = PdsSerializer
    name = 'pds-list'

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        owner_id = self.request.user
        return Pds.objects.filter(owner=owner_id)


class PdsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Pds.objects.all()
    serializer_class = PdsSerializer
    name = 'pds-detail'
    permission_classes = (isOwnerOrDenied, )

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance,
                                         data={'last_update': datetime.now()},
                                         partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)
