from django.conf.urls import url
from app import views
from django.urls import include
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

'''
course-ratings/                         -> Visualizza tutti i ratings fatti da user
course-ratings/?course={id del corso}   -> Visualizza tutti i ratings di un certo corso fatti da user
course-ratings/{id rating}              -> Visualizza il singolo rating con quell'id
'''

urlpatterns = [
    url(r'^cdls/$', views.CdlList.as_view(), name=views.CdlList.name),
    url(r'^cdls/(?P<pk>[0-9]+)/$',
        views.CdlDetail.as_view(), name=views.CdlDetail.name),
    url(r'^courses/$', views.CourseList.as_view(),
        name=views.CourseList.name),
    url(r'^courses/(?P<pk>[0-9]+)/$',
        views.CourseDetail.as_view(), name=views.CourseDetail.name),
    url(r'^restricted-courses/(?P<pk>[0-9]+)/$',
        views.CdlCourseDetail.as_view(), name=views.CdlCourseDetail.name),
    url(r'^instructors/(?P<pk>[0-9]+)/$',
        views.InstructorDetail.as_view(), name=views.InstructorDetail.name),
    url(r'^instructors/$', views.InstructorList.as_view(),
        name=views.InstructorList.name),
    url(r'^course-ratings/$', views.CourseRatingsList.as_view(),
        name=views.CourseRatingsList.name),
    url(r'^course-ratings/(?P<pk>[0-9]+)/$',
        views.CourseRatingsDetail.as_view(),
        name=views.CourseRatingsDetail.name),
    url(r'^instructor-ratings/$',
        views.InstructorRatingsList.as_view(),
        name=views.InstructorRatingsList.name),
    url(r'^instructor-ratings/(?P<pk>[0-9]+)/$',
        views.InstructorRatingsDetail.as_view(),
        name=views.InstructorRatingsDetail.name),
    url(r'^users/$', views.UserDetail.as_view(),
        name=views.UserDetail.name),
    url(r'^pds/$', views.PdsList.as_view(), name=views.PdsList.name),
    url(r'^pds/(?P<pk>[0-9]+)/$',
        views.PdsDetail.as_view(), name=views.PdsDetail.name),

    # endpoint rest-auth
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),

    # jwt
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),

    # simple-jwt
    url(r'^api/token/$', TokenObtainPairView.as_view(),
        name='token_obtain_pair'),
    url(r'^api/token/refresh/$', TokenRefreshView.as_view(),
        name='token_refresh'),
    url(r'^api/token/verify/$', TokenVerifyView.as_view(),
        name='token_verify'),

    # API Root
    url(r'^$', views.ApiRoot.as_view(), name=views.ApiRoot.name)
]
