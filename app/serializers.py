from drf_dynamic_fields import DynamicFieldsMixin
from rest_framework import serializers
from rest_framework.exceptions import ParseError
# from django.db import models
from django.contrib.auth.models import User

from app.models import (Cdl, Course, CourseRating, Instructor,
                        InstructorRating, Pds)


class CourseRatingSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    course = serializers.ReadOnlyField(source='course.title')
    url = serializers.HyperlinkedIdentityField(
        view_name='course-rating-detail')

    # comment = serializers.TextField()
    class Meta:
        model = CourseRating
        fields = ('url', 'id', 'stars', 'comment', 'created', 'owner',
                  'course', 'cfu', 'academic_year')


class InstructorRatingSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    instructor = serializers.ReadOnlyField(source='instructor.name')
    url = serializers.HyperlinkedIdentityField(
        view_name='instructor-rating-detail')

    class Meta:
        model = InstructorRating
        fields = ('url', 'id', 'stars', 'comment', 'created',
                  'owner', 'instructor')


class CdlListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cdl
        fields = ('url', 'id', 'name')


class InstructorSerializer(DynamicFieldsMixin,
                           serializers.HyperlinkedModelSerializer):
    courses = serializers.SerializerMethodField()

    class Meta:
        model = Instructor
        fields = ('url', 'id', 'name', 'courses', 'stars')

    def get_courses(self, obj):
        courses = Instructor.objects.get(id=obj.id).course_set.all()
        return CourseListSerializer(
            courses,
            many=True,
            context={'request': self.context['request']}).data


class CourseDetailSerializer(DynamicFieldsMixin,
                             serializers.HyperlinkedModelSerializer):
    # instructors = serializers.PrimaryKeyRelatedField(
    #    many=True, queryset=Instructor.objects.all())
    instructors = InstructorSerializer(read_only=True, many=True)
    cdl = serializers.CharField(source='cdl.name')

    class Meta:
        model = Course
        fields = ('url', 'id', 'instructors', 'academic_year', 'cfu', 'cdl',
                  'title', 'code', 'department', 'sector', 'teaching_period',
                  'teaching_schedule', 'mutuazioni', 'content', 'textbooks',
                  'objectives', 'methods', 'exam', 'program', 'stars')


class CourseListSerializer(serializers.HyperlinkedModelSerializer):
    # instructors = serializers.PrimaryKeyRelatedField(
    #    many=True, queryset=Instructor.objects.all())

    class Meta:
        model = Course
        fields = ('url', 'id', 'cfu', 'title', 'code', 'stars')


# class CourseRatingsListSerializer(serializers.HyperlinkedModelSerializer):
#     ratings = CourseRatingSerializer(read_only=True, many=True)
#     class Meta:
#         model = Course
#         fields = ('url', 'id', 'ratings')


class CdlDetailSerializer(serializers.HyperlinkedModelSerializer):
    # courses = serializers.HyperlinkedRelatedField(
    # many=True, read_only=True, view_name='cdl-course-detail')

    # courses = CourseListSerializer(read_only=True, many=True)

    courses = serializers.SerializerMethodField()

    class Meta:
        model = Cdl
        fields = ('url', 'id', 'name', 'courses')

    def get_courses(self, obj):
        request = self.context['request']
        params = getattr(request, 'query_params', None)
        courses = Course.objects.filter(cdl=obj.id)  # Base queryset

        # If there are params, try to search for academic_year.
        # Else, leave defeult base queryset.
        if params:
            try:
                academic_year = params['academic_year']
            except(KeyError):
                raise ParseError(
                    "Accepted query params:  academic_year: string")

            # Valid query param academic_year: return only courses with chosen
            # academic_year.
            courses = courses.filter(academic_year=academic_year)

        return CourseListSerializer(
            courses,
            many=True,
            context={'request': request}).data


class PdsSerializer(DynamicFieldsMixin,
                    serializers.HyperlinkedModelSerializer):
    # cdl = serializers.CharField(source='cdl.name')
    cdl = serializers.PrimaryKeyRelatedField(
        many=False, queryset=Cdl.objects.all())
    # cdl = CdlSerializer()
    owner = serializers.ReadOnlyField(source='owner.username')
    # courses = CourseListSerializer(read_only=False, many=True)
    courses = serializers.PrimaryKeyRelatedField(
        many=True,
        read_only=False,
        queryset=Course.objects.all())
    last_update = serializers.DateTimeField(required=False, read_only=False)

    class Meta:
        model = Pds
        fields = ('url', 'id', 'name', 'courses', 'owner',
                  'cdl', 'last_update')  # courses come list

    def to_representation(self, obj):
        data = super(PdsSerializer, self).to_representation(obj)
        if 'courses' in data:
            courses = Course.objects.filter(id__in=obj.courses.all())
            data['courses'] = CourseListSerializer(
                courses,
                many=True,
                context={'request': self.context['request']}).data
        return data


class UserSerializer(serializers.HyperlinkedModelSerializer):
    # course_ratings = serializers.HyperlinkedRelatedField(
        # many=True, read_only=True, view_name='user-detail')

    class Meta:
        model = User
        fields = ('username', 'email', 'id')
