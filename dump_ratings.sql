insert into app_course_rating (created, stars, comment, cfu,
                               academic_year, course_id, owner_id)
values
(current_timestamp, '2.5', 'a comment', '6', '2017/2018',
 (select min(id) from app_course where title='OPTIMIZATION OF COMPLEX SYSTEMS'),
 (select id from auth_user where username='username1')
);

insert into app_course_rating (created, stars, comment, cfu,
                               academic_year, course_id, owner_id)
values
(current_timestamp, '4.5', 'a comment', '9', '2017/2018',
 (select min(id) from app_course where title='PARALLEL COMPUTING'),
 (select id from auth_user where username='username3')
);

insert into app_course_rating (created, stars, comment, cfu,
                               academic_year, course_id, owner_id)
values
(current_timestamp, '3.5', 'a comment', '6', '2017/2018',
 (select min(id) from app_course where title='MACHINE LEARNING'),
 (select id from auth_user where username='username3')
);

insert into app_course_rating (created, stars, comment, cfu,
                               academic_year, course_id, owner_id)
values
(current_timestamp, '1.5', 'a comment', '9', '2017/2018',
 (select min(id) from app_course where title='SECURITY AND NETWORK MANAGEMENT'),
 (select id from auth_user where username='username4')
);

insert into app_course_rating (created, stars, comment, cfu,
                               academic_year, course_id, owner_id)
values
(current_timestamp, '3.5', 'a comment', '9', '2017/2018',
 (select min(id) from app_course where title='DATA WAREHOUSING'),
 (select id from auth_user where username='username4')
);


values
(current_timestamp, '2.5', 'a comment', '9', '2017/2018',
 (select min(id) from app_course where title='DATA WAREHOUSING'),
 (select id from auth_user where username='username5')
);


values
(current_timestamp, '2.5', 'a comment', '9', '2017/2018',
 (select min(id) from app_course where title='TELECOMMUNICATION NETWORKS'),
 (select id from auth_user where username='username6')
);


values
(current_timestamp, '3.5', 'a comment', '9', '2017/2018',
 (select min(id) from app_course where title='DATA AND DOCUMENT MINING'),
 (select id from auth_user where username='username7')
);
