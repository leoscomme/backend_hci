--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: app_cdl; Type: TABLE DATA; Schema: public; Owner: leonardo
--

INSERT INTO public.app_cdl (id, name) VALUES (1, 'Corso di Laurea Magistrale in INGEGNERIA INFORMATICA');


--
-- Data for Name: app_course; Type: TABLE DATA; Schema: public; Owner: leonardo
--

INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (1, '2017/2018', 1, 'ADVANCED NUMERICAL ANALYSIS', 'B024332', 'Ingegneria dell''Informazione', 'MAT/08 - ANALISI NUMERICA', 'Primo Anno - Primo Semestre', 'dal 18/09/2017 al 22/12/2017', '6', '*MANCANTE*', 'Metodi di interpolazione ed approssimazione. Interpolazione polinomiale di Hermite e per curve. Le funzioni Splines. Approssimazione di Berstein. La migliore approssimazione ai minimi quadrati. La derivazione numerica. Le formule di quadratura in generale e quelle interpolatorie. MATLAB.', 'L. Gori, Calcolo Numerico IV edizione 2006, Edizioni KAPPA
F. Fontanella, A. Pasquali, Calcolo numerico, Metodi e algoritmi. Vol. 2, 1980, Pitagora Editore
M.L. Lo Cascio, Fondamenti di Analisi Numerica, 2007, McGraw-Hill Bevilacqua , Bini , Capovani, Menchi, Metodi Numerici, 1992, Zanichelli', 'Saper riconoscere e risolvere un problema di natura numerica ed in particolare un problema di approssimazione.
Individuare strategie algoritmiche risolutive.', 'Lezione frontale ed esercitazione Matlab in laboratorio sugli argomenti visti a lezione', 'Esame orale volto anche alla verifca di competenze critiche e trasversali ed alla capacita''di sviluppare algoritmi Matlab per la risoluzione di problemi di analisi numerica', '[1] Approssimazione ed interpolazione:
[1.1] Posizione del problema: classi di funzioni e forma delle possibili approssimanti;
[1.2] Il polinomio interpolante nella forma di Lagrange; Espressione dell''errore;
[1.3] L''errore nel caso dei nodi uniformi ed il suo comportamento asintotico;
[1.4] Stabilita'' nelle formule di interpolazione e la costante di Lebesgue; 
[1.5] I polinomi di Chebyshev; Interpolazione con nodi gli zeri di Chebyshev;
[1.6] Il Teorema di Weierstrass ed in polinomi di Berstein;
[1.7] Polinomi interpolanti di tipo osculatorio ed interpolazione di Hermite; Espressione dell''errore;
[1.8] Le funzioni Splines: definizione, prorieta'', base delle potenze troncate;
[1.9] Spline interpolanti ed approssimanti; Le spline cubiche interpolanti nei nodi (naturali e complete)
1.10] Le B-spline come base dello spazio delle spline e l''algoritmo di De Boor (con particolare attenzione al caso cubico);
[1.1] Il caso parametrico: interpolazione parametrica con parametrizzazione uniforme e della lunghezza dell''arco;
[2] Sistemi lineari rettangolari: il problema lineare dei minimi quadrati min_{x\in \RR^n}||Ax-b||_2 caso m>> n
[2.1] Posizione del problema; Esistenza ed unicita'' della soluzione;
[2.2] Risoluzione mediante il sistema delle equazioni normali A^TAx=A^Tb;
[2.3] Matrici ortogonali e loro proprieta''; le matrici di Hauseholder;
[2.4] Fattorizzazione QR di una matrice utilizzando le matrici di Hauseholder;
[2.5] Risoluzione del problema lineare dei minimi quadrati utilizzando la fattorizzazione QR;
[2.6] La migliore approssimazione ai minimi quadrati trigonometrica ed il caso particolare dell''interpolazione; sviluppo di Fourier: cenni;
[3] Derivazione numerica: idee di base ed alcune semplici formule. Il metodo dei coefficienti indeterminati;
[4] Formule di quadratura (FdQ)
[4.1] Posizione del problema; caso lineare sui nodi x_0,...,x_n: formule del tipo \sum_{i=0}^nw_if(x_i)
[4.2] Grado di precisione g per le FdQ; limitazione superiore del grado di precisione (GdP) g
[4.3] Caso pesi equilimitati: dimostrazione di convergenze delle FdQ all''integrale per n->\infty e studio della stabilita'' della formula;
[4.4] Metodo dei coefficienti indeterminati;
[4.5] FdQ interpolatorie: generalita'' e limite inferiore del grado di precisione (n
[4.5.1] Formule di Newton-Cotes di tipo aperto; grado di precisione ed esempi;
[4.5.2] Formule di Newton-Cotes di tipo chiso; grado di precisione ed esempi;
[4.5.4] Valutazione pratica dell''errore e metodo di estrapolazione di Richardson;
[4.5.5] Formule di quadratura adattative;', 'https://www.ing-inm.unifi.it/p-ins2-2017-485236-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (2, '2017/2018', 1, 'ADVANCED PROGRAMMING TECHNIQUES', 'B027202', 'Ingegneria dell''Informazione', 'INF/01 - INFORMATICA', 'Primo Anno - Primo Semestre', 'dal 18/09/2017 al 22/12/2017', '6', 'Insegnamento mutuato da:
B027540 - ADVANCED TECHNIQUES AND TOOLS FOR SOFTWARE DEVELOPMENT
Corso di Laurea Magistrale in INFORMATICA
Curricula RESILIENT AND SECURE CYBER PHYSICAL SYSTEMS', 'Test automation dei programmi e Test-Driven Development sotto vari aspetti. Build Automation e Continuous Integration, usando il "cloud". Tutti questi strumenti saranno "collegati" insieme, per un processo automatizzato di sviluppo e rilascio. Saranno utilizzati anche strumenti che analizzano la qualità del codice. Tutto il processo è pensato per lo sviluppo "collaborativo". Useremo strumenti che virtualizzano l''ambiente e per una facile riproducibilità dei test.', 'Materiale didattico disponibile su https://e-l.unifi.it/course/view.php?id=4537', 'Conoscenza e comprensione: Il corso ha lo scopo di insegnare tecniche basate sul test automatico dei programmi (Test-Driven Development) sotto vari aspetti: Unit Testing (testare un singolo componente), Integration Testing (testare più componenti insieme) e Functional Testing (testare l''interfaccia utente, sia essa un programma in esecuzione sul PC o un''interfaccia Web). I test rappresenteranno un meccanismo per sviluppare molto più velocemente anche i programmi più complessi, usando fin dall''inizio una metodologia che renderà i programmi modulari. I test daranno una misura della qualità del software prodotto e permetteranno di testarlo in modo automatico.  Infine, i test possono essere visti come strumenti di specifica del comportamento del software.
Inoltre, saranno mostrati strumenti di versionamento del codice (git, sia in locale che distribuito su Internet, Github) e per la Build Automation dei programmi, cioè la compilazione automatica di tutta l''applicazione e l''esecuzione di tutti i test dell''applicazione.  Questi saranno usati nel contesto della Continuous Integration, affidandosi a server dedicati (ad es., Jenkins) e disponibili anche nel cloud, in modo gratuito (ad es., Travis-CI).  Tutti questi strumenti saranno "collegati" insieme, per avere un processo automatizzato di sviluppo che consente il continuo monitoraggio del codice sviluppato e facilita la release dei programmi (Continuous Delivery).  In questo processo saranno utilizzati anche strumenti che analizzano il codice e rilevano eventuali problemi di qualità che riguardano la "pulizia" del codice. Tutto il processo è pensato per lo sviluppo "collaborativo", cioè all''interno di un team di sviluppo.
Oltre agli strumenti gratuiti e open source e quelli disponibili sul cloud, useremo anche strumenti che virtualizzano l''ambiente e permettono una facile riproducibilità del contesto in cui eseguire i test dell''applicazione (ad es., Docker).  Questi permetteranno anche di testare facilmente l''applicazione in più contesti (ad es., usando differenti versioni delle librerie e server di terze parti che sono utilizzati dalla nostra applicazione).
Durante tutto il corso ci affideremo a strumenti che incrementano la produttività, in primis useremo un IDE (Eclipse).
Tutte le tecniche e gli strumenti saranno infine applicati allo sviluppo di applicazioni web in Java(*), testando i singoli componenti e il sito web finale.  Per questa parte utilizzeremo come framework Spring Boot.
(*non incluse nel corso a 6 CFU)
Capacità di applicare conoscenza e comprensione: Alla fine del corso gli studenti saranno in grado di sviluppare applicazioni guidate dai test e di scrivere test sotto i vari aspetti (unit test, integration test, functional test) e di metter su un meccanismo di Continuous Integration e Build Automation che tiene traccia dei vari test eseguiti e che analizza la qualità del codice.', 'lezioni in aula e alcune esercitazioni in laboratorio', 'La verifica finale consta di un progetto da svolgere a casa e da un esame orale.
Il progetto viene proposto dallo studente (o dal gruppo di studenti) e concordato col docente, utilizzando le tecniche mostrate durante il corso.
Il progetto è mirato a dimostrare le capacità di sviluppo guidato dai test, di testare ogni componente dell''applicazione, l''integrazione di vari componenti e l''applicazione finale.  Per il corso da 9 CFU, l''applicazione svolta nel progetto dovrà essere un''applicazione web.
L''orale consisterà nella discussione del progetto svolto e in varie domande su tutti i contenuti del corso.
Il voto finale terrà conto della qualità del progetto e delle risposte all''esame orale.', 'Il corso ha lo scopo di insegnare tecniche basate sul test automatico dei programmi (Test-Driven Development) sotto vari aspetti: Unit Testing (testare un singolo componente), Integration Testing (testare più componenti insieme) e Functional Testing (testare l''interfaccia utente, sia essa un programma in esecuzione sul PC o un''interfaccia Web).  Inoltre, saranno mostrati strumenti di versionamento del codice (git, sia in locale che distribuito su Internet, Github) e per la Build Automation dei programmi, cioè la compilazione automatica di tutta l''applicazione e l''esecuzione di tutti i test dell''applicazione. Questi saranno usati nel contesto della Continuous Integration, affidandosi a server dedicati (ad es., Jenkins) e disponibili anche nel cloud, in modo gratuito (ad es., Travis-CI). Tutti questi strumenti saranno "collegati" insieme, per avere un processo automatizzato di sviluppo che consente il continuo monitoraggio del codice sviluppato e facilita la release dei programmi (Continuous Delivery). In questo processo saranno utilizzati anche strumenti che analizzano il codice e rilevano eventuali problemi di qualità che riguardano la "pulizia" del codice. Tutto il processo è pensato per lo sviluppo "collaborativo", cioè all''interno di un team di sviluppo. Oltre agli strumenti gratuiti e open source e quelli disponibili sul cloud, useremo anche strumenti che virtualizzano l''ambiente e permettono una facile riproducibilità del contesto in cui eseguire i test dell''applicazione (ad es., Docker). Questi permetteranno anche di testare facilmente l''applicazione in più contesti (ad es., usando differenti versioni delle librerie e server di terze parti che sono utilizzati dalla nostra applicazione). Durante tutto il corso ci affideremo a strumenti che incrementano la produttività, in primis useremo un IDE (Eclipse).', 'https://www.ing-inm.unifi.it/p-ins2-2017-485238-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (3, '2017/2018', 1, 'COMPUTER GRAPHICS AND 3D', 'B024312/B024311', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Primo Anno - Secondo Semestre', 'dal 26/02/2018 al 08/06/2018', '6/9', '*MANCANTE*', '1. Computer graphics
The computer graphics pipeline will be presented and analyzed, by focusing on the aspects of modeling, animation and rendering of a 3D scene. Practical examples will be given using OpenGL and Matlab code.
2. 3D acquisition and processing 
We will focus on the acquisition and processing of 3D real data. Algorithms and methods will be addressed for concrete applications, like 3D retrieval, 3D recognition, 3D biometrics, etc.', '[1] Steven J. Gortler, "Foundations of 3D Computer  Graphics," The MIT Press, 2012
[2] John F. Hughes et al., "Computer  Graphics,   Principles and Practice," Wiley and Sons, Third   Edition, 2014
[3] Graham Sellers et al. "OpenGL SUPERBIBLE", Addison Wesley, Seventh Edition, 2015', 'L''obiettivo del corso è duplice: 
- da un lato, verrà presentata una completa introduzione a concetti di base ed avanzati della grafica computazionale, presentando le più comuni tecniche per la modellazione 3D, l''animazione ed il rendering;
- dall''altro, saranno considerati gli aspetti specifici relativi alla acquisizione ed elaborazione di dati 3D reali acquisiti con scanner sia a bassa che ad alta-risoluzione, con l''obiettivo di presentare anche applicazioni pratiche, come riconoscimento e ricerca di oggetti 3D, biometria da dati 3D, etc. Sarà anche data una breve introduzione ai dispositivi di stampa 3D.
La teoria ed i concetti presentati nel corso saranno applicati utilizzando codice OpenGL e Matlab.', 'Lezione frontale in aula con uso di slide ed alla lavagna. Esercitazioni in laboratorio (programmazione C++ e Javascript con API OpenGL e Matlab)', 'La valutazione è basata su:
- un elaborato di programmazione su argomenti di computer graphics o 3D da eseguire in gruppi da 1 a 3 persone. L''elaborato ha l''obiettivo di valutare la comprensione delle tecniche e dei metodi principali di Comptuer Graphics e di elaborazione di dati 3D introdotti durante il corso e la capacità di tradurre gli stessi in una idea e realizzazione progettuale.
- esame orale su un sottoinsieme degli argomenti del corso. La prova orale mira a valutare la comprensione delle tecniche di computer graphics presentate nel corso e la capacità di applicarle a casi concreti. La valutazione è basata su:
- un elaborato di programmazione su argomenti di computer graphics o 3D da eseguire in gruppi da 1 a 3 persone. L''elaborato ha l''obiettivo di valutare la comprensione delle tecniche e dei metodi principali di Comptuer Graphics e di elaborazione di dati 3D introdotti durante il corso e la capacità di tradurre gli stessi in una idea e realizzazione progettuale. 
- esame orale su un sottoinsieme degli argomenti del corso. La prova orale mira a valutare la comprensione delle tecniche di Computer Graphics presentate nel corso e la capacità di applicarle a casi concreti. La prova orale mira inoltre a verificare le capacità di esposizione, analisi e sintesi del candidato.', '1. Computer graphics
In the first part of the course, the computer graphics pipeline will be presented and analyzed, by focusing on the aspects of modeling, animation and rendering of a 3D scene. Some hints of the 2D case will be also given. Practical examples on the different subjects will be given using OpenGL and Matlab code. The same programming frameworks will be used for experimental activities (laboratory work and assignments).
Modeling
Essential mathematics and the geometry of 2D-space and 3D-space
- Curves and surfaces: Bézier curves and splines
A simple way to describe shape in 2D and 3D
- "Meshes" in 2D: polylines
- Meshes in 3D: manifold and non-manifold meshes, basic mesh operations
Curve Properties & Conversion, Surface Representations
Coordinates and Transformations: Homogeneous coordinates, Perspective
Hierarchical models: Hierarchical grouping of objects (scene graph), Hierarchical Modeling in OpenGL
Animation
Articulated models: Joints and bones, skeleton hierarchy, forward kinematics, inverse kinematics
Color: Spectra, Cones and spectral response, Color blindness and metamers, Color matching, Color spaces
Character Animation: Animate simple "skeleton", Attach "skin" to skeleton
Basics of Computer Animation: Keyframing, Procedural, Physically-based 
Animation Controls 
Character Animation using skinning/enveloping
Collision detection and response: Point-object and object-object detection, Only point-object response
Rendering
Ray Casting
Rasterization
Ray Tracing: Shade (interaction of light and material), Secondary rays (shadows, reflection, refraction)
Textures and Shading
Texture Mapping & Shaders: Sampling & Antialiasing, Shadows, Global illumination
OpenGL
Introduction to the Open Graphics Library: Industry standard graphics library used to produce real-time 2D and 3D graphics
2. 3D acquisition and processing 
In the second part of the course, we will focus on the acquisition and processing of 3D real data (both with low and high resolution). Algorithms and methods will be addressed for concrete applications, like 3D retrieval, 3D recognition, 3D biometrics, etc. 
3D acquisition
High resolution 3D scanners technology, Low resolution 3D cameras (Kinect)
Algorithms on the mesh
Tangent, Normal, Curvature (mean, Gaussian, principal)
Geodesic computation on the mesh: Dijkstra, Fast marching algorithm
3D descriptors on the mesh: shape-index, shape-context, mesh-SIFT, mesh-HOG, mesh-LBP
Laplace-Beltrami operator
Applications
3D objects retrieval, 3D recognition (static and dynamic), 3D biometrics
Modern graphics hardware
Graphics Processing Unit (GPU) for real-time 3D computer graphics
GPU programming with CUDA
3D printing
Basic technologies and applications', 'https://www.ing-inm.unifi.it/p-ins2-2017-485232-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (4, '2017/2018', 1, 'DATA AND DOCUMENT MINING', 'B024276/B024275', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Primo Anno - Primo Semestre', 'dal 18/09/2017 al 22/12/2017', '6/9', '*MANCANTE*', 'Data Mining
Document Engineering
Document Image Analysis
Information Retrieval', '(I) C.D. Manning, P. Raghavan, P. Raghavan Introduction to Information Retrieval, Cambridge University Press - 2008
(I) A. Rajaraman, J. D. Ullman, Mining of Massive Datasets, 2011
(L) I. Witten, A. Moffat, T.C. Bell Managing Gigabytes, Van Nostrand Reinhold – 1999
(L) D. Doermann, K. Tombre (Eds.) Handbook of Document Image Processing and Recognition, 2014 (L)
Note:
(L) : Book available in the Engineering library
(I) : Book available in Internet (authors'' version)', 'Obiettivo del corso è quello di fornire le conoscenze e capacità necessarie a progettare e sviluppare sistemi che permettano di estrarre conoscenza da grandi quantità di dati con particolar riferimento ad applicazioni nell''ambito di sistemi di analisi di immagini di documenti.
- Conoscenza delle tecniche di base del Data Mining che consentono di modellare grandi 
quantità di dati ed estrarne informazione utile.
- Conoscenza delle problematiche relative all''estrazione di informazione ed indicizzazioni di documenti sia testuali che non testuali. 
- Conoscenza dei principali modelli e algoritmi in Information Retrieval 
- Conoscenza  delle principali tecniche per l''estrazione di informazioni da documenti digitalizzati e quindi acquisite prevalentemente sotto forma di immagini.', 'Lezioni frontali, esercitazioni in classe, svolgimento assistito
di elaborati.', 'Durante il corso è prevista l''analisi di un articolo scientifico e la sua presentazione ai colleghi durante le lezioni
La verifica finale consta di un elaborato e di una prova orale.
L''elaborato può essere o individuale e basato sullo studio di alcuni articoli scientifici o in gruppo e finalizzato all''implementazione al test e all''analisi di una semplice applicazione nell''ambito degli argomenti del corso.
Nel complesso la verifica deve permettere allo studente di mostrare le capacità di:
  - Saper analizzare un problema pratico e progettare una sua soluzione
  - Saper applicare le principali tecniche descritte durante il corso tramite implementazione di un modulo software o tramite analisi teorica
  - Saper interagire con colleghi per portare avanti lo svolgimento del progetto
  - Saper descrivere in modo accurato in forma scritta il lavoro svolto e fornire una appropriata analisi dei risultati
  - Saper descrivere metodi ed algoritmi degli argomenti trattati nel corso', 'Basic concepts of Secondary Storage
Large scale file systems
Map-reduce, algorithms using Map-reduce
Information Retrieval
Document Engineering 
Document Image Analysis and Recognition 
Data Mining
Finding Similar Items, Frequent itemsets, Clustering, High-dimensional spaces and dimensionality reduction, Web mining, Datawarehouse
Homework & project', 'https://www.ing-inm.unifi.it/p-ins2-2017-485231-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (22, '2017/2018', 1, 'SOFTWARE DEPENDABILITY', 'B024322/B024321', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6/9', '*MANCANTE*', 'Richiami di dependability
Software Reliability
Algoritmi distribuiti
Logica temporale e verifica formale Interpretazione astratta e Analisi statica Applicazioni delle tecniche di Verifica Formale Software product lines
Normative, contatti con le industrie', 'A. Fantechi - Informatica Industriale - Citta? Studi Edizioni
Dispense prodotte dal docente, disponibili sulla pagina web del docente', 'Il corso si propone di illustrare una serie di tecniche che consentono di ovviare per quanto possibile al problema dell''introduzione di errori di progetto nella produzione del software; quindi, principalmente, tecniche di verifica formale e di sviluppo formale del software, ma anche tecniche di previsione dei guasti e di tolleranza ai guasti. Il corso mira in particolare a fornire:
Conoscenza della disciplina della software reliability
Conoscenza di algoritmi distribuiti per tolleranza ai guasti
Conoscenza dei principali metodi formali 
Conoscenza dei principi del model-based design
Conoscenza della teoria e delle tecniche della verifica formale di sistemi software
Conoscenza di tecniche di valutazione quantitativa di attributi di dependability basate su modelli stocastici *
Capacità di impostare un processo di produzione del software mirato alla sua dependability
Capacità di modellare correttamente il comportamento di un semplice sistema
Capacità di utilizzare strumenti di model-based design
Capacità di utilizzare strumenti per la verifica formale 
Capacità di utilizzare strumenti di mediazione stocastica *
(*non incluse nel corso a 6 CFU)', 'Lezioni in aula, esercitazioni personali in laboratorio', 'La verifica finale consta di un elaborato assegnato dal docente, di norma ad un gruppo formato da due o tre studenti, e da una prova orale.
 L''elaborato è mirato a dimostrare le capacità di:
- Saper modellare un sistema distribuito di media complessità secondo i canoni del model-based design
- Saper applicare strumenti di model-based design e di verifica formale sul modello oggetto dell''elaborato
- Saper definire un modello stocastico astraendo correttamente le caratteristiche salienti dal modello del sistema *
- Saper utilizzare uno strumento di valutazione quantitativa sul modello stocastico ottenuto *
(*non incluse nel corso a 6 CFU)
L''orale è mirato a valutare le conoscenze personali acquisite:
Conoscenza della disciplina della software reliability
Conoscenza di algoritmi distribuiti per tolleranza ai guasti
Conoscenza della teoria della modellazione formale e della verifica formale di sistemi software', 'Il corso eroga 9 CFU.
C''è la possibilità di frequentare e dare l''esame per 6 CFU, seguendo solo il primo modulo.
Modulo 1 (6 CFU)
Richiami sui concetti di dependability di sistemi controllati da computer
Misure per la software dependability
Software reliability: modelli di stima
Tolleranza ai guasti software: Forward/Backward error recovery
Algoritmi distribuiti - concetti di consistenza, validity e agreement
Memoria stabile
Checkpointing distribuito ed effetto domino
Two-phase commit protocol
Principio di incertezza
Paradosso dei generali bizantini
Byzantine Agreement: l''algoritmo ZA.
L''algoritmo di consistenza interattiva
Algoritmi di sincronizzazione di clock distribuiti
Metodi Formali per lo sviluppo e la verifica del software
Metodi assiomatici, Z, B
Logica Modale
Logica Temporale
LTL - proprietà safety/liveness, proprietà di fairness , precedenza (until)
Proprietà di ricorrenza, minimo e massimo punto fisso
Logiche branching - CTL - interpretazione su Kripke Structures - CTL*
Algoritmo di Model Checking per CTL
Esplosione dello spazio degli stati
I Binary Decision Diagram (BDD) come tecnica di memorizzazione compatta dello spazio degli stati
Algoritmo di model checking basato su punto fisso
Buchi automata - algoritmo di model checking per LTL
Labelled Transition Systems vs. Kripke Structures
Equivalenza di bisimulazione forte, Equivalenza osservazionale
Logica HML
Local model checking
Algebre di processi - CCS - Semantica operazionale - Equivalenza osservazionale
Logica ACTL
Model Driven Development
I diagrammi di stato UML
Model checking su diagrammi a stati UML e su Statecharts
Modellazione di un sistema con Statecharts: i dialetti di Stateflow e Scade
Strumenti di model checking: SMV, SPIN, UMC
Esercitazioni di modellazione e verifica formale
Modulo 2 (3 CFU)
Analisi statica del codice: L''interpretazione astratta.
Data-Flow Testing.
Software model checking. 
Concetti di astrazione. 
Counterexample-Guided Abstraction Refinement.
Ingegneria dei requisiti.', 'https://www.ing-inm.unifi.it/p-ins2-2017-485262-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (5, '2017/2018', 1, 'DATA SECURITY AND PRIVACY', 'B027204', 'Ingegneria dell''Informazione', 'INF/01 - INFORMATICA', 'Primo Anno - Secondo Semestre', 'dal 26/02/2018 al 08/06/2018', '6', 'Insegnamento mutuato da:
B027502 - DATA SECURITY AND PRIVACY
Corso di Laurea Magistrale in INFORMATICA
Curricula DATA SCIENCE', 'Network security. Crittografia a chiave condivisa. Cifrari perfetti secondo Shannon, One-Time-Pad, unicity distance. Cifrari di Feistel. Crittografia a chiave pubblica. Elementi di aritmetica modulare. I cifrari RSA e El Gamal, il protocollo di Diffie-Hellman. Funzioni hash one-way crittografiche. Autenticazione e firma digitale.
Elementi di Teoria dell''Informazione e codici.
Privacy. k-anonymity e attacchi background knowledge.', '1) Michele Boreale. Note per il corso di Codici e Sicurezza.
Disponibile in rete.
2) Articoli e materiale forniti dal docente.', 'Conoscenze - Il corso mira a fornire agli studenti una comprensione approfondita dei principi scientifici alla base della trasmissione efficiente, affidabile e sicura dei dati.
Competenze - 
Alla fine del corso, lo studente dovrebbe saper padroneggiare e applicare i principi teorici fondamentali (matematici, crittografici ed algoritmici) alla base della protezione dei dati e della Privacy.
Capacità – Alla fine del corso, lo studente avrà acquisito  la capacità di costruire modelli ad alto livello, ma rigorosi, dei sistemi di comunicazione, ed analizzare le criticità dal punto di vista della Sicurezza.', 'Lezioni frontali.', 'L''esame consiste di due parti:
1. Svolgimento di un progetto, che può essere o di tipo programmativo o di approfondimento teorico, centrato su argomenti inerenti i codici e la sicurezza dei dati (*);
2. prova orale sugli argomenti del corso e sul progetto svolto.
(*) esempi di svolgimento di tali progetti sono resi disponibili attraverso la piattaforma di e-learning.', 'Network security. Crittografia a chiave condivisa. Cifrari perfetti secondo Shannon, One-Time-Pad, unicity distance. Cifrari di Feistel. Crittografia a chiave pubblica. Elementi di aritmetica modulare. I cifrari RSA e El Gamal, il protocollo di Diffie-Hellman. Funzioni hash one-way crittografiche. Autenticazione e firma digitale
Elementi di Teoria dell''Informazione. Codici di compressione: 1^ Shannon, codici Huffman. Canali con rumore, capacità e codici rilevatori e correttori. 
Privacy: k-anonimity, l-diversity, t-closeness. Background information. Differential Privacy. (*)
Approfondimenti di crittografia: il cifrario AES; prova di correttezza del test di Miller-Rabin; key equivocation e unicity distance; schemi undeniable di firma digitale. (*)
(*) Non incluso nei corsi mutuati da 6 CFU.', 'https://www.ing-inm.unifi.it/p-ins2-2017-485239-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (6, '2017/2018', 1, 'DATA WAREHOUSING', 'B027206', 'Ingegneria dell''Informazione', 'INF/01 - INFORMATICA', 'Primo Anno - Secondo Semestre', 'dal 26/02/2018 al 08/06/2018', '6', 'Insegnamento mutuato da:
B014443 - DATA WAREHOUSING
Corso di Laurea Magistrale in INFORMATICA
Curricula DATA SCIENCE', 'Il corso si propone di presentare allo studente gli strumenti e le metodologie per l''analisi dei dati a supporto del processo decisionale. Modellazioni concettuali e modelli semantici, Business Intelligence e sistemi di supporto alle decisioni. Sistemi informativi per metadati, Sistemi informativi per la rappresentazione dei processi, l''approccio relazione per la rappresentazione dello spazio e del tempo  Applicazioni OLTP e applicazioni OLAP.', 'Matteo Golfarelli, Stefano Rizzi Data Warehouse - Teoria e pratica della progettazione 3/ed 
Mc-Graw-Hill
Materiale predisposto dai docenti', 'Acquisire capacità e competenze di analisi e di rappresentazione dei problemi. 
Competenze di gestione di sistemi informativi a supporto delle decisioni,  conoscenza delle tecniche di progettazione e gestione di basi dati storici e di data warehouse', 'Lezioni frontali', 'Discussione di una breve relazione 
Verifica orale', '• dal linguaggio ai modelli informativi
• temporal reasoning
• la dimensione spaziale nell''ambito dei sistemi informativi a supporto delle decisioni e della conoscenza
• il ruolo delle ontologie nella predisposizione dei sistemi informativi a supporto delle decisioni
• modelli informativi per la rappresentazione dei processi
• oltre il relazionale- modelli concettuali per dati aggregati
• modellazione di sistemi informativi a supporto del monitoraggio delle politiche 
• Business Intelligence e sistemi di supporto alle decisioni.
• Applicazioni OLTP e applicazioni OLAP.
• Architetture e progettazione di un Data Mart
• Strumenti ETL
• Modello multidimensionale.
• Strumenti di reportistica avanzata, navigazione Olap
• Panoramica delle tecniche di Data Mining.', 'https://www.ing-inm.unifi.it/p-ins2-2017-485240-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (7, '2017/2018', 1, 'IMAGE AND VIDEO ANALYSIS', 'B024271/B024269', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Primo Anno - Secondo Semestre', 'dal 26/02/2018 al 08/06/2018', '6/9', '*MANCANTE*', '.', '- Computer Vision: Models, Learning, and Inference
Simon J. D. Prince
University College London
- Computer and Machine Vision: Theory, Algorithms, Practicalities
E. R. Davies
Academic Press
- Digital Image Processing
R.C. Gonzalez, R.E. Woods
Ed. Pearson, Prentice Hall
- Materiale disponibile sul sito web del docente www.dsi.unifi.it/pala/
- Altro materiale
www.icaen.uiowa.edu/~dip/LECTURE/contents.html', 'Obiettivo del corso è quello di fornire le conoscenze e competenze
necessarie ad effettuare operazioni di elaborazione ed analisi di immagini e video.  
- Conoscenza dei modelli per la descrizione di caratteristiche globali e locali delle immagini in termini di colore e tessitura
- Conoscenza di modelli per la rappresentazione multiscala del contenuto di immagini
- Conoscenza dei modelli per la sogliatura e segmentazione di immagini in base a caratteristiche locali
- Conoscenza di modelli per la descrizione della dinamica in un video
- Capacità di applicare le conoscenze acquisite per il progetto di moduli software per l''analisi del contenuto di immagini e video.', 'Lezioni in aula ed esercitazioni in laboratorio con Matlab', 'L''esame si svolge attraverso una prova orale, eventualmente affiancata dallo sviluppo di un elaborato. Nella prova viene verificata la capacità comunicativa del candidato e la conoscenza dei modelli di descrizione di immagini e video e la capacità di formalizzare in forma algoritmica o con pseudocodice la struttura di un modulo di analisi di immagini/video con riferimento a problemi di segmentazione di immagine e stima del flusso ottico.', 'Proprietà metriche, distanza di Hausdorff, distanza di Mahalanobis, proprietà topologiche.
Trasformazioni puntuali: istogramma, espansione di scala, equalizzazione di immagine: modelli globali e locali
Trasformazioni position dependent; Local histogram equalization ; trasformazioni geometriche: applicazione e stima;  image morphing
Operatori locali: introduzione; Smoothing: media, gaussiano; bilateral filter, filtro mediano; Correlazione e template matching; 
Concetto di edge; edge e derivate; operatori del primo ordine; maschere di roberts, prewit, sobel; operatori del secondo ordine; Laplaciano, LoG, DoG; Filtro di canny: principi, soppressione non massimale, isteresi; Contronto risultati edge su immagine con rumore; edge detection con modelli parametrici; estrazione edge su immagini a colori; 
Corner detection: modello di Harris
Rappresentazione multiscala e scale-space: motivazioni e definizione del modello, N-jet ed invarianti; Anisotropic diffusion models;
Invarianti di colore; 
Descrizione tessiture: introduzione, motivazione e contesti applicativi; matrici co-occorrenza, matrice di covarianza; Tamura, Local Binary Patterns, Fourier Power Spectrum, Region covariance, Gabor, Momenti; 
La Gestalt ed i principi di raggruppamento; Sogliatura di immagini con il metodo di Otsu;  Sogliatura immagini rumorose, sogliatura con media mobile; 
Binary mathematical morphology: erode and dilate;  open, close, smooth, hit-or-miss, gradient, hole filling; binary reconstruction by dilation and erosion, open by reconstruction, ultimate erosion, distance transform. 
Lattice morphology: erosion, dilation, open, close, morphological gradient, top-hat, bottom-hat, contrast enhancement, grayscale reconstruction, opening by reconstruction; Granulometry with MM; 
Segmentazione di immagini attraverso clustering;  Clustering gerarchico, K-means
Gaussian Mixtures, Algoritmo EM; Mean Shift; 
Segmentazione basata su grafi: Normalized Cuts; 
Edge-based segmentation: Hough rette, rette con parametrizzazione theta; Hough cerchi e generalizzata; Watersheds
Principio di formazione del moto sul piano immagine. Applicazioni ed esempi pratici di analisi del moto.  Relazione tra 3D motion field e 2D motion field.
Flusso ottico. Definizione e relazione con 2D motion field. Casi particolari e degeneri. Distinzione tra metodi densi e metodi sparsi. Stima del flusso ottico tramite metodi differenziali: algoritmo Horn&Schunk.
Stima del flusso ottico tramite il metodo di Lucas e Kanade. Stima del flusso con modello di moto globale affine: l''algoritmo KLT. Stima in caso di displacement elevato: algoritmo KLT piramidale, metodo di Brox e Malik basato su Region Matching.
Segmentazione del moto. Casi possibili (telecamera fissa, brandeggiabile, in movimento). Segmentazione background/foreground. Metodi dinamici di modellazione del background basati su modelli statistici. Algoritmo Mixture of Gaussian.', 'https://www.ing-inm.unifi.it/p-ins2-2017-485230-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (8, '2017/2018', 1, 'INFORMATION THEORY', 'B026371', 'Ingegneria dell''Informazione', 'ING-INF/03 - TELECOMUNICAZIONI', 'Primo Anno - Primo Semestre', 'dal 18/09/2017 al 22/12/2017', '6', 'Insegnamento mutuato da:
B019012 - TEORIA DELL''INFORMAZIONE
Corso di Laurea Magistrale in INGEGNERIA DELLE TELECOMUNICAZIONI', 'Il corso affronta il problema di trasmettere efficacemente e fedelmente un messaggio. Introduce l''informazione come qualcosa di definito e misurabile e fornisce risposta a due quesiti fondamentali per qualsiasi sistema di informazione, ovvero quale è il massimo livello di compressione e quale è la massima velocità di trasmissione dei dati. Il corso fornisce poi elementi di codifica sia di sorgente che di canale, utilizzate per avvicinarsi ai limiti teorici  definiti in precedenza.', 'Il materiale didattico dell''intero corso è disponibile sulla piattaforma Moodle.
Come ulteriori testi di consultazione per eventuali approfondimenti:
T. M. Cover, J. A. Thomas: Elements of Information Theory. John Wiley & Sons, New York,  2nd ed 2006
N. Abramson: Information Theory and Coding. McGraw-Hill, New York, 1963.
S. Lin, D. J. Costello Jr.: Error Control Coding: Fundamentals and Applications. Prentice-Hall, 1983.
K. Sayood: Introduction to data compression, Elsevier 4a ed., 2012
S. Benedetto, E. Biglieri, V. Castellani: Digital Transmission Theory, Prentice Hall, 1988.
J. G. Proakis: Digital Communications. McGraw-Hill, 4a Ed., 2001.
A. Papoulis, S.U. Pillai, Probability, Random Variables, and Stochastic Processes, 4th ed., McGraw-Hill, 2002.', 'Il corso ha lo scopo di fornire le conoscenze di base per la rappresentazione in forma compatta dell''informazione e la trasmissione affidabile dell''informazione su un canale di comunicazione con rumore, in particolare:
- Conoscenze dei concetti di base sulla rappresentazione dell''informazione
- Conoscenza degli elementi fondamentali della codifica di sorgente e tecniche di codifica di sorgente
- Conoscenza delle problematiche e caratterizzazione della trasmissione affidabile su canali rumorosi
- Conoscenza sui principi della codifica di canale e tecniche di codifica di canale.
Al termine del corso, lo studente avrà acquisito la capacità di: 
- comprendere i principi alla base degli standard correnti per la compressione di dati, audio, immagini e video 
- saper applicare tecniche di compressione di sorgente
- comprendere i principi alla base delle tecniche di protezione di un segnale trasmesso su un canale con rumore
- saper applicare tecniche di codifica di canale', 'l corso sarà costituito da:
Lezioni frontali svolte con l''ausilio di dispense fornite dal docente (messe a disposizione sulla piattaforma Moodle)
Seminari finali su nuovi ambiti applicativi della Teoria dell''Informazione', 'La verifica finale consta di una prova orale durante la quale verranno fatte domande teoriche e svolti esercizi.
Le domande teoriche hanno lo scopo di verificare
- la comprensione del concetto di informazione
- la comprensione della teoria che sta alla base della compressione e della codifica di canale
- la comprensione dei meccanismi e dei principi che permettono di costruire sia un codice di sorgente che di canale
- la comprensione delle caratteristiche dei codici di sorgente e di canale e le metriche di valutazione delle prestazioni
Gli esercizi hanno lo scopo di verificare
- la capacità di applicare i concetti teorici
- la capacità di applicare le tecniche di codifica analizzate', 'PARTE I –  INTRODUZIONE - Misura dell''informazione ed Entropia. Entropia di sorgenti discrete, sorgenti senza memoria e con memoria, sorgenti continue. 
PARTE II – COMPRESSIONE DI SORGENTE- Introduzione alla codifica di sorgente, classificazione dei codici. Disuguaglianze di Kraft e di McMillan. Lunghezza media di un codice. Primo teorema di Shannon.  Codifica di Huffman. Codifica aritmetica. Codifica di Lempel-Ziv, Codifica Run-Length
PARTE III- RATE DISTORTION THEORY- Quantizzazione, distorsione e misura della distorsione. Teoria della Rate-Distortion (cenni): significato della curva di rate-distortion di una sorgente. Funzione di rate distortion per variabili Gaussiane
PARTE IV - CAPACITA'' DI CANALE - Modelli di canale per la trasmissione di informazione. Equivocazione di canale. Capacità di canale. Regole di decisione e probabilità di errore. Disuguaglianza di Fano. Secondo teorema di Shannon, sulla trasmissione affidabile di informazione su canali rumorosi. Capacità di un canale Gaussiano. Limite di Shannon e regione di comunicazione affidabile. 
PARTE V - CODIFICA DI CANALE - Codifica a controllo d''errore. Rivelazione e correzione di errori. Codici blocco. Codici lineari. Decodifica hard di codici lineari. Codici ciclici. Codici BCH. Codici Reed-Solomon. Codici concatenati. Tecniche di interleaving. Codici convoluzionali. Decodifica dei codici convoluzionali: algoritmo di Viterbi. Decodifica soft. Guadagno di un codice di canale.
Seminari (i) su Turbo Codifica e LDPC (ii) su physical layer security', 'https://www.ing-inm.unifi.it/p-ins2-2017-485235-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (47, '2018/2019', 1, 'MULTIAGENT SYSTEMS', 'B028465', 'Ingegneria dell''Informazione', 'ING-INF/04 - AUTOMATICA', 'Secondo Anno - ', 'dal  al ', '6', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502801-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (48, '2018/2019', 1, 'NETWORKS AND TECHNOLOGIES FOR SMART SYSTEMS', 'B027209', 'Ingegneria dell''Informazione', 'ING-INF/03 - TELECOMUNICAZIONI', 'Secondo Anno - ', 'dal  al ', '6', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502790-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (49, '2018/2019', 1, 'PROVA FINALE', 'B003623', 'Ingegneria dell''Informazione', 'PROFIN_S - ', 'Secondo Anno - ', 'dal  al ', '21', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502805-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (9, '2017/2018', 1, 'OPTIMIZATION METHODS', 'B024333', 'Ingegneria dell''Informazione', 'MAT/09 - RICERCA OPERATIVA', 'Primo Anno - Primo Semestre', 'dal 18/09/2017 al 22/12/2017', '6', '*MANCANTE*', 'Condizioni di ottimalità
Metodi per l''ottimizzazione locale non vincolata
Metodi per l''ottimizzazione locale vincolata
Metodi di ottimizzazione per problemi di apprendimento automatico', 'Metodi di ottimizzazione non vincolata, L. Grippo, M. Sciandrone, Springer-Verlag, 2011
Dispense', 'Comprendere e saper utilizzare le condizioni di ottimalità; conoscere i principali approcci algoritmici per l''ottimizzazione locale e le loro proprietà teoriche e computazionali', 'Lezioni frontali. Le lezioni vengono registrate (audio e video) e rese disponibilit tramite moodle', 'Esame scritto o orale (in alternativa) su tutto il programma.
Nel corso dell''esame  si verifica mediante quesiti e domande teoriche:
- la conoscenza della teoria dell''ottimizzazione (condizioni di ottimalità)
- la conoscenza di applicazioni dell''ottimizzazione al machine learning
- la conoscenza di algritmi di ottimizzazione non lineare', 'Introduzione: modelli di ottimizzazione, esempi
Nozioni di base e definizioni. 
Condizioni di ottimalità di KKT
Introduzione ai problemi di machine learning
Convergenza degli algoritmi
Ottimizzazione mono-dimensionale
Metodi di discesa al gradiente
Metodi di Newton
Metodi alle direzioni coniugate
Metodi Quasi-Newton
Metodi Trust region
Metodi per problemi vincolati', 'https://www.ing-inm.unifi.it/p-ins2-2017-485237-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (10, '2017/2018', 1, 'OPTIMIZATION OF COMPLEX SYSTEMS', 'B024334', 'Ingegneria dell''Informazione', 'MAT/09 - RICERCA OPERATIVA', 'Primo Anno - Secondo Semestre', 'dal 26/02/2018 al 08/06/2018', '6', '*MANCANTE*', 'Metodi di decomposizione. Ottimizzazione multiobiettivo. Giochi ed equilibri. Ottimizzazione sparsa.Ot.timizzazione multiobiettivo', 'Dispense.', '1) Capacità di formulare problemi di ottimizzazione non lineare.
2) Conoscenza di algoritmi per problemi di ottimizzazione a larga scala, di ottimizzazione sparsa, di equilibrio di reti, di apprendimento automatico, di ottimizzazione multi-agente, di ottimizzazione multiobiettivo.
3) Capacità di applicare e adattare, a specifici contesti applicativi,  algoritmi standard di ottimizzazione non lineare.', 'Lezioni frontali', 'L''esame consta di una prova scritta (o alternativamente di una prova orale) nella quale si verifica mediante quesiti e domande teoriche:
- la capacità di formulare problemi di ottimizzazione;
- la capacità di utilizzare e adattare algoritmi standard di ottimizzazione; 
- la conoscenza di algoritmi per la soluzione di specifiche classi di problemi complessi di ottimizzazione.', 'Metodi di decomposizione per ottimizzazione non vincolata e ottimizzazione vincolata. Metodi di decomposizione per l''apprendimento automatico. Metodi della discesa più ripida per problemi di ottimizzzazione multiobiettivo. Giochi ed equilibri di Nash. Algoritmi esatti per l''ottimizzazione globale. Euristiche per problemi di ottimizzazione globale. Metodi di tipo LASSO per l''ottimizzazione sparsa. Metodi di programmazione concava per la minimizzazione in norma zero.
Algoritmi per ottimizzazione multiobiettivo.', 'https://www.ing-inm.unifi.it/p-ins2-2017-485243-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (11, '2017/2018', 1, 'PARALLEL COMPUTING', 'B024314/B024313', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Primo Anno - Primo Semestre', 'dal 18/09/2017 al 22/12/2017', '6/9', '*MANCANTE*', '*MANCANTE*', '- Principles of Parallel Programming, Calvin Lyn and Lawrence Snyder, Pearson
- Parallel Programming for Multicore and Cluster Systems, Thomas Dauber and Gudula Rünger, Springer
- Programming Massively Parallel Processors, David B. Kirk and Wen-mei W. Hwu, Morgan Kaufmann
- An introduction to Parallel Programming, Peter Pacheco, Morgan Kaufmann', 'Scopo del corso è introdurre gli studenti alle tecniche di programmazione parallela e ad alta performance.
Al termine del corso lo studente possiede le basi fondamentali di programmazione parallela per sistemi multicore, cluster e GPGPU, conosce inoltre i principali paradigmi di programmazione parallela e gli ambienti di programmazione standard Java, C++11, Pthreads, OpenMP, MPI e CUDA.', 'Lezioni frontali (80%) e attività di laboratorio (20%)', '- Progetto di sviluppo software durante il corso (40% del voto finale)
- Progetto di sviluppo software finale (60% del voto finale)
Per ogni progetto deve essere scritta una relazione tecnica ed una presentazione che descrive il lavoro e riporta la performance rispetto ad una versione sequenziale del progetto.
Gli elaborati sono scelti dagli studenti da una lista proposta dal docente. Possono essere svolti singolarmente o in coppia.
L''elaborato è mirato a dimostrare le capacità di:
- Saper implementare un software parallelo usando uno (corso a 6 crediti) o due (corso a 9 crediti) dei framework e strumenti visti a lezione
- Saper valutare gli effetti e differenze della programmazione parallela rispetto a quella sequenziale
- Saper misurare la performance di un programma parallelo rispetto ad uno sequenziale
- Saper scrivere una relazione tecnica ed effettuare una presentazione tecnica.', 'Livelli di parallelismo (istruzioni, transazioni, task, thread, memoria.)
Modelli di parallelismo (SIMD, MIMD, SPMD)
CPU e architetture parallele
Design Pattern per Programmazione concorrente (Master/Worker; Message passing)
Parallelization strategies, task parallelism, data parallelism, e work sharing
Programmazione parallela in C/C++ (C++11)/Java
Strutture dati concorrenti
Multi-core processor programming
Shared memory parallelism; OpenMP
Multithreading
Distributed network programming
Distributed memory model; MPI
Hadoop; Apache Storm e Lambda architecture
Overview GPGPU, Hardware GPU
CUDA; compilatore CUDA e strumenti
La memoria nella GPU e suo accesso
Stream e multi-GPU
Utilizzo librerie CUDA', 'https://www.ing-inm.unifi.it/p-ins2-2017-485242-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (12, '2017/2018', 1, 'COMPUTATIONAL VISION', 'B024316/B024315', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6/9', '*MANCANTE*', '1. LA VISIONE NELL''UOMO E NELLA MACCHINA: ASPETTI COMPUTAZIONALI
2. FORMAZIONE DELL''IMMAGINE
3. VISIONE MONOCULARE: VISTE DI SINGOLI PIANI
4. REGISTRAZIONE DI IMMAGINI
5. TELECAMERE: MODELLI E CALIBRAZIONE
6. Ricostruzione 3D densa da sequenze video.
7. ALGORITMI DI STEREOPSI
8. RICOSTRUZIONE 3D DA VISTE SINGOLE E MULTIPLE
9. APPLICAZIONI: Beni culturali, Realta'' aumentata, INTERAZIONE UOMO-MACCHINA, 
ROBOTICA avanzata, MULTIMEDIA, Informatica forense, etc.', '1. Hartley and Zisserman, MULTIPLE VIEW GEOMETRY IN COMPUTER VISION. 
Cambridge University Press, 2003 (2nd edition).
2. Visione Computazionale - Tecniche di ricostruzione tridimensionale. Franco Angeli 2013.
Su diversi argomenti saranno messi a disposizione
degli studenti sia appunti di lezione che articoli in lingua inglese tratti dalla
letteratura recente.', 'Il corso intende dotare lo studente di strumenti teorici e pratici
per l''analisi computazionale di immagini singole e sequenze video.
Tali conoscenze sono finalizzate allo sviluppo di moderni sistemi di computer vision 
2D e 3D, con applicazioni nei più svariati campi, quali
la robotica autonoma e la guida automatica, la fruizione e preservazione dei beni culturali, 
gli ausili per disabili, l''automazione industriale, l''interazione avanzata uomo-macchina basata su movimenti e gesti,
la grafica 2D/3D interattiva ed adattativa, l''informatica forense.', 'Lezioni in aula con lavagna e videoproiettore. Sperimentazioni in aula con computer 
portatili e rete wireless.', 'Esame orale per tutti gli studenti (6 e 9 CFU). Il colloquio prende avvio dalla teoria di uno degli argomenti in programma, che si chiede di illustrare in dettaglio. Seguono domande di ordine pratico-realizzativo, ed esercizi volti a verificare la comprensione della teoria come strumento per risolvere problemi reali. 
Elaborato o ricerca bibliografica (solo studenti 9 CFU). L''elaborato verte sulla scrittura di un programma (in MATLAB, C++ o Python) per la soluzione di un semplice problema attraverso le tecniche studiate nel corso o altre tecniche che il candidato desideri approfondire. Al programma deve essere allegata una relazione che descriva i dettagli matematici e di implementazione degli algoritmi impiegati. La ricerca bibliografica (anch''essa proponibile dallo stesso studente) consiste in una review di una particolare tecnica di visione (es. algoritmi per il calcolo della disparita'' stereo) alla luce della letteratura recente, ovvero nell''approfondimento di un ambito applicativo della visione (es. algoritmi di visione per l''industria cinematografica).', '1. LA VISIONE NELL''UOMO E NELLA MACCHINA: ASPETTI COMPUTAZIONALI
Introduzione al corso. Ambiguita'' in visione. Illusioni visive. Il ruolo della semantica nella percezione. Indizi 3D in un''immagine.
2. FORMAZIONE DELL''IMMAGINE
L''immagine come sintesi di luce, materiale e geometria da
parte di un osservatore. Tipi di superfici. BRDF.
Albedo. Componenti diffusa e speculare. Dispositivi
di acquisizione delle immagini. Ottiche.
3. VISIONE MONOCULARE: VISTE DI SINGOLI PIANI. Omografie e loro anatomia. Rettificazione di immagini basata sui punti circolari.
4. REGISTRAZIONE DI IMMAGINI
Stima robista di omografie: RANSAC etc. Mosaici. Mosaicing in presenza di parallasse. Image-based 
rendering. Tecniche super-risoluzione.
5. TELECAMERE: MODELLI E CALIBRAZIONE
Camera a foro stenopeico, natural camera, affine camera. 
Distorsione radiale. Calibrazione fotogrammetrica.
Autocalibrazione.
6. ANALISI DI SEQUENZE VIDEO
Optical flow vs motion field. Structure from motion (caso
continuo). Tempo all''impatto. SLAM (simultaneous localization and mapping). 
7. ALGORITMI DI STEREOPSI
Geometria di due viste. Matrice fondamentale ed essenziale. Parallasse.
Rettificazione di una coppia stereo. Ricostruzione proiettiva e metrica.
Disparità. Algoritmi per
lo stereo denso.
8. RICOSTRUZIONE DA VISTE SINGOLE E MULTIPLE
Pipeline di ricostruzione da viste multiple. Bundle adjustment. Ricostruzione da viste singole:
vincoli sulla scena (piani, superfici di rivoluzione, etc.).
Metrologia da una vista singola.
9. APPLICAZIONI: Interfacce naturali uomo-macchina, Esterocepsi per robot, Post-produzione video, videoproiettori intelligenti, fotografia computazionale, televisione 3D, informatica forense, etc.', 'https://www.ing-inm.unifi.it/p-ins2-2017-485252-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (50, '2018/2019', 1, 'SECURITY AND NETWORK MANAGEMENT', 'B024337', 'Ingegneria dell''Informazione', 'ING-INF/03 - TELECOMUNICAZIONI', 'Secondo Anno - ', 'dal  al ', '6', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502803-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (51, '2018/2019', 1, 'SOFTWARE ARCHITECTURES AND METHODOLOGIES', 'B024308/B024307', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - ', 'dal  al ', '6/9', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502807-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (52, '2018/2019', 1, 'SOFTWARE DEPENDABILITY', 'B024322/B024321', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - ', 'dal  al ', '6/9', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502809-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (13, '2017/2018', 1, 'HUMAN COMPUTER INTERACTION', 'B024324/B024323', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6/9', '*MANCANTE*', 'Il corso è organizzato intorno a degli argomenti tecnici e
teorici sulla Human Computer Interaction. Più dettagli sotto
nella sezione della Programma del Corso.', 'Libri:
A. Cooper, R. Reimann, D. Cronin (2007). About Face 3: The Essentials of Interaction Design. Indianapolis, Indiana: Wiley.
Norman, D. A. (2013). The design of everyday things: Revised and expanded edition. Basic books.
Articoli scientifici selezionati da convegni e  riviste sulla HCI:
CHI: https://chi2015.acm.org/
ACM TOCHI: http://tochi.acm.org/', 'Obiettivo del corso è quello di fornire le conoscenze e capacità necessarie a progettare e sviluppare sistemi con concentrazione sull''interazione macchina-uomo (HCI).
  - Conoscenza dei concetti psicologici importanti per l''HCI: affordance, signifier, mapping, teoria di percezione Gestalt, e la psicologia delle azioni.
  - Conoscenza dei modelli e paradigmi di programmazione più utilizzati nell''HCI.
  - Conoscenze delle tecniche principali di needfinding utilizzate per stabilire requisiti funzionali dei sistemi HCI.
  - Conoscenza dei design framework principali attualmente in uso.
  - Conoscenza della panoramica dell''HCI nel mondo accademico e nel mondo industriale.', 'Lezioni frontali e sessioni di laboratorio.', 'La verifica finale consta di una prova orale in base a un compito di programmazione e un elaborato che dimostrano le capacità di:
  - Saper progettare e sviluppare interfacce grafiche utilizzando la programmazione orientata ad eventi.
  - Saper applicare i principi di separazione di responsibilità e MVC nel contesto di applicazioni GUI.
  - Saper progettare e eseguire processi di needfinding per individuare gli personas e scenari di uso.
  - Saper sviluppare le proprie idee dell''elaborato indipendentemente o in un piccolo gruppo.
  - Saper progettare e effettuare test di usabilità.
  - Saper comunicare in forma scritta e orale le motivazioni, implementazione, e esiti del elaborato.', 'The course is organized around the following technical and theoretical topics:
- Needfinding: activity and cognitive task analysis, establishing design goals.
- Prototyping: storyboarding, paper and digital mock-ups, high-fidelity digital prototypes.
- Programming models for HCI: events and managing asynchronicity, the model-view-controller model
- Usability assessment: testing, metrics, heuristic evaluation, user studies, 
- Platforms: mobile, desktop, large surface, wearable.
- Technical topics: python programming for user interfaces, the Kivy framework, managing asynchronicity, rapid prototyping tools.
- Advanced theoretical, applied, and emerging topics: natural interaction, tangible interaction, Kinect, the Internet of Things (IoT), functional reactive programming, gamification, etc.
Please note that this list of topics is tentative and will be finalized before the beginning of the semester.', 'https://www.ing-inm.unifi.it/p-ins2-2017-485253-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (14, '2017/2018', 1, 'IMAGE PROCESSING AND SECURITY', 'B024336', 'Ingegneria dell''Informazione', 'ING-INF/03 - TELECOMUNICAZIONI', 'Secondo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6', 'Insegnamento mutuato da:
B010498 - ELABORAZIONE E PROTEZIONE DELLE IMMAGINI
Corso di Laurea Magistrale in INGEGNERIA DELLE TELECOMUNICAZIONI
Curricula RETI E TECNOLOGIE INTERNET', 'Il programma del corso comprende i seguenti argomenti:
* Acquisizione e rappresentazione immagini 
* Operatori spaziali
* Trasformate numeriche 
* Standard di compressione JPEG e MPEG
* Tecniche di image forensics
* Crittografia, cifratura di immagini', 'Oltre alle slide disponibili, ulteriori fonti informative sono i seguenti libri:
* R. Gonzalez, R. Woods, "Digital Image Processing", Prentice Hall.
* R. Gonzalez, R. Woods, S. Eddins , "Digital Image Processing Using
MATLAB", Prentice Hall, 2004
* W. Stallings, Crittografia e Sicurezza delle Reti, Mc Graw Hill', 'Obiettivo del corso è quello di fornire allo studente le conoscenze di base riguardo alle tecniche di elaborazione delle immagini e alle tecniche per garantire requisiti di sicurezza quali l''autenticazione e
la riservatezza delle immagini.
In particolare, intende: 
* Fornire gli strumenti metodologici di base per la descrizione e l''analisi delle immagini digitali. 
* Introdurre gli operatori spaziali e in frequenza per l''elaborazione di immagini. 
* Spiegare i principi di compressione di immagini e video. 
* Fornire una panoramica dei sistemi di protezione delle immagini.
Con tali conoscenze, lo studente acquisirà:
* La capacità di analizzare e caratterizzare, sia nel dominio del tempo che nel dominio della frequenza, le immagini digitali. 
* La capacità di interpretare gli effetti dei principali operatori spaziali e in frequenza per l''elaborazione di immagini. 
* La capacità di implementare semplici sistemi - sia attivi che passivi - per l''autenticazione e la protezione delle immagini digitali.', 'Il corso sarà costituito da lezioni teoriche frontali e da
esercitazioni pratiche svolte presso il laboratorio LESC.
Nell''ambito del corso verranno poi tenuti dei seminari su tematiche
specifiche', 'L''esame consiste in una prova orale, volta ad accertare l''apprendimento di:
- tecniche per il modellamento e l''analisi delle immagini digitali;
- metodi per l''elaborazione di immagini, sia nel dominio spaziale che in frequenza, e per la compressione; 
- tecniche di image forensics per determinare l''origine e l''integrità di un''immagine digitale.', 'Vengono qui riportati gli argomenti trattati.
Introduction 
* What is Digital Image Processing
* Digital Image Fundamentals 
Intensity Transformations and Spatial Filtering 
* Basic Spatial Domain Processing
* Point Processing
* Histogram processing
* Spatial Enhancement Methods
Digital Transforms 
* Fourier Transform 
* Sampling
* Discrete Fourier Transform and its properties
* Interpolation in image processing
* Filtering in the Frequency Domain
* Discrete Cosine Transform 
Compression standards: JPEG and MPEG 
* Redundancies in visual data
* Some basic compression methods
* JPEG Compression Standard
* MPEG compression
Image Forensics 
* Authentication of multimedia contents
* Image Forensics 
* Digital Camera Model
* Acquisition based Image Forensics
* Coding based Image Forensics
* Editing based Image Forensics
Cryptography 
* Framework and Basic Terminology
* Symmetric Cryptography; block ciphers, stream ciphers
* Asymmetric Cryptography; RSA
* Authentication: Hash, Digital Signatures
* Image Encryption', 'https://www.ing-inm.unifi.it/p-ins2-2017-485254-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (15, '2017/2018', 1, 'KNOWLEDGE MANAGEMENT AND PROTECTION SYSTEMS', 'B026368/B026367', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6/9', 'Insegnamento mutuato da:
B028462 - BIG DATA ARCHITECTURES
Corso di Laurea Magistrale in INGEGNERIA INFORMATICA', '-- big data concepts
-- big data stores
-- cloud management: virtual machine e container
-- Big data computing
-- Performance analysis
-- IOT architecture and solutions
-- big data analytic
-- Big data architectures
Durante il corso saranno effettuate esercitazioni su cluster hadoop, mapreduce, IOT application, containers, elastic search, twitter vigilance.', 'vari', 'imparare ad progettare, usare e comprendere architetture big data', 'slides and exercitations in the lab.
Durante il corso saranno effettuate esercitazioni su cluster hadoop, mapreduce, IOT application, containers, elastic search, twitter vigilance.', 'small project', '-- big data concepts
-- big data stores
-- cloud management: virtual machine e container
-- Big data computing
-- Performance analysis
-- IOT architecture and solutions
-- big data analytic
-- Big data architectures', 'https://www.ing-inm.unifi.it/p-ins2-2017-485250-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (16, '2017/2018', 1, 'LABORATORIO/TIROCINIO', 'B010516', 'Ingegneria dell''Informazione', 'NN - ', 'Secondo Anno - ', 'dal  al ', '3', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2017-485245-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (17, '2017/2018', 1, 'MACHINE LEARNING', 'B024318/B024317', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6/9', '*MANCANTE*', '', '', 'You will learn about several fundamental and some advanced algorithms for statistical learning, you will know the basics of computational learning theory, and will be able to design state-of-the-art solutions to application problems.', 'Lectures and practical sessions.', '', 'Please see', 'https://www.ing-inm.unifi.it/p-ins2-2017-485248-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (18, '2017/2018', 1, 'NETWORKS AND TECHNOLOGIES FOR SMART SYSTEMS', 'B027209', 'Ingegneria dell''Informazione', 'ING-INF/03 - TELECOMUNICAZIONI', 'Secondo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6', 'Insegnamento mutuato da:
B027067 - RETI E TECNOLOGIE PER SISTEMI INTELLIGENTI
Corso di Laurea Magistrale in INGEGNERIA DELLE TELECOMUNICAZIONI
Curricula RETI E TECNOLOGIE INTERNET', '- Panoramica su paradigmi di Internet of Things, Cloud Computing, 5G e relativi requisiti di gestione del traffico
- cloud computing
- Internet of Things
- programmabilità delle reti: Software Defined Networking e Network Function Virtualization', 'Foundations of Modern Networking, William Stallings, Pearson', 'Il corso ha l''obiettivo di fornire gli elementi di base per la comprensione e l''utilizzo delle moderne architetture di reti e computing, basate su tecnologie di virtualizzazione, orchestrazione e Internet of Things. In particolare verranno presentati moderni paradigmi, quali cloud computing, Internet of Things, programmabilità delle reti tramite tecnologie di software defined networking (SDN) e virtualizzazione di funzioni di rete (Network Function Virtualization).
Nel corso verranno introdotti i principi di base delle architetture cloud e Internet of Things. Verranno inoltre analizzati i requisiti di gestione di risorse di computing e networking introdotti da queste architetture e, in prospettiva, dalla loro adozione sistematica nelle architetture di rete mobile di 5° generazione (5G).  Partendo dall''analisi dell''attuale domanda di traffico nei suddetti ambiti  e delle limitazioni degli approcci di networking tradizionali, saranno illustrati i paradigmi di software defined networking e virtualizzazione delle funzioni di rete. Oltre alla presentazione di aspetti teorici e specifiche standard, saranno mostrati anche alcuni software open source (un controller di rete, un orchestratore di funzioni virtuali e un cloud management system). 
Il corso ha l''obiettivo di formare figure professionali che coniughino conoscenze di tipo sistemistico e di programmazione e sappiano applicarle nell''ambito della gestione di reti e, più in generale, sistemi pervasivi, considerando anche problematiche di business. Il corso offre competenze di interesse per gli ambiti di Cloud Computing, Internet of Things, Software Defined Networking.', 'Didattica frontale
Seminari tematici di approfondimento
La presentazione degli aspetti teorici è integrata con l''illustrazione di esempi pratici', 'L''esame si svolge attraverso un colloquio orale, eventualmente affiancato dallo sviluppo e presentazione di un elaborato. Nella prova viene verificata la conoscenza delle tecnologie e modelli di programmabilità delle reti e architetture avanzate di networking e relative applicazioni previsti nel programma di studio; capacità di analisi e valutazione dell''applicazione dei suddetti modelli e tecnologie; capacità di applicare praticamente le conoscenze acquisite (attraverso elaborato o domande sulle implementazioni tecnologiche presentate nel corso).', '- Panoramica su paradigmi di Internet of Things, Cloud Computing, 5G
- Analisi delle tipologie di traffico e limiti delle architetture di networking attuali
- Cloud Computing: Concetti di base, modelli di servizi cloud (Software as a Service, Platfrma as a Service, Infrastructure as a Service), modalità di deployment, architettura cloud di riferimento.
- Internet of Things: architetture di riferimento e componenti principali
- Software defined networking: limitazioni delle attuali tecnologie di rete e principi dell''approccio SDN; SDN data plane e protocollo OpenFlow; SDN Control Plane (architettura generale dei controllori SDN, analisi di controllori open source, API REST nei controllori, architetture centralizzate e distribuite dei control plane); SDN Application Plane e applicazioni principali di SDN (traffic engineering, monitoraggio, data center networking)
- Network Function Virtualization: principi di base, Architettura di gestione delle risorse cloud per servizi di rete, integrazione SDN-NFV
- Scenari futuri: Mobile Edge Computing in reti 5G e Fog Computing
In base all''interesse degli studenti saranno svolti seminari su utilizzo di controller SDN (OpenDayLight o ONOS) e cloud management system (OpenStack) per la gestione delle funzioni virtuali di rete', 'https://www.ing-inm.unifi.it/p-ins2-2017-485255-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (19, '2017/2018', 1, 'PROVA FINALE', 'B003623', 'Ingegneria dell''Informazione', 'PROFIN_S - ', 'Secondo Anno - ', 'dal  al ', '21', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2017-485244-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (20, '2017/2018', 1, 'SECURITY AND NETWORK MANAGEMENT', 'B024337', 'Ingegneria dell''Informazione', 'ING-INF/03 - TELECOMUNICAZIONI', 'Secondo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6', 'Insegnamento mutuato da:
B024343 - SICUREZZA E GESTIONE DELLE RETI
Corso di Laurea Magistrale in INGEGNERIA DELLE TELECOMUNICAZIONI
Curricula RETI E TECNOLOGIE INTERNET', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2017-485256-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (21, '2017/2018', 1, 'SOFTWARE ARCHITECTURES AND METHODOLOGIES', 'B024308/B024307', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6/9', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2017-485259-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (23, '2017/2018', 1, 'TELECOMMUNICATION NETWORKS', 'B024339', 'Ingegneria dell''Informazione', 'ING-INF/03 - TELECOMUNICAZIONI', 'Secondo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6', 'Insegnamento mutuato da:
B027065 - SISTEMI A CODA
Corso di Laurea Magistrale in INGEGNERIA DELLE TELECOMUNICAZIONI
Curricula RETI E TECNOLOGIE INTERNET', 'SISTEMI A CODA (9 CFU)
MODULO :
RETI DI TELECOMUNICAZIONI (6CFU)
TELECOMMUNICATION NETWORKS (6CFU)
Modelli di sistemi a coda base ed avanzati;
 Sistemi a coda con priorità di servizio;
 Reti di code di tipo aperto e chiuso;
 Allocazione ottima di capacità in reti di telecomunicazione;
MODULO APPLICAZIONI (3 CFU).
Tecniche di istradamento;
Tecniche per il controllo della congestione;
Reti Wireless;
Software Defined Networks;
Applicazioni industriali : industria 4.0
Tecnologia 5G', 'Testo di riferimento del corso:
R. Fantacci, Sistemi a Coda, Modelli, Analisi e Applicazioni, Esculapio, 2015
Materiale di Supporto didattico: 
Appunti dalle lezioni ed esercitazioni.
Testi di Cosultazione.
L. Kleinrock, Sistemi a Coda, Hoepli, 1990
D. Bersekas, R. Gallager, Data Networks, Prentice Hall, 1992.
M. Schwartz , Telecommunication Networks, Addison Wesley, 1987
J.F. Hayes, Modelling and Analysis of Telecommunications Networks, Plenum Press, 2004.
K.S. Trivedi, Probability and Statitistics with Reliability Queueing and Computer Science Applications, Wiley, 2002.
Hwei Hsu, Probabilità, variabili casuali e processi stocacstici, McGraw-Hill, 2011.', 'Il corso è stato concepito in accordo con il recente riordino degli studi con l''obiettivo principale di fornire conoscenze di base ed avanzate dei sistemi a Coda e, più in generale, della Teoria delle Code.  Esso è rivolto principalmente, ma non esclusivamente, agli studenti di corsi di Laurea Magistrale in Informatica, Ingegneria dell''Informazione e Matematica.  Attraverso gli argomenti trattati saranno acquisite capacità di applicare conoscenza e comprensione per la definizione di modelli e la specifica di metodologie di analisi proprie di sistemi a coda di base e complessi,  anche in relazione a specifici contesti applicativi riguardanti i settori tecnologici delle reti di telecomunicazione e dei sistemi per elaborazione dell''informazione.', 'Lezioni ed esercitazioni frontali. Seminari di approfondimento tenuti da qualificato personale esterno.', 'La prova di esame prevede un colloquio orale riguardo tutti gli argomenti prevsiti dal programma di studio.
In particolare le domande saranno finalizzate a verificare:
- Conoscenza dei modelli dei sistemi a coda prevsiti dal programma di studio;.
- Capacità di applicare modelli di teria delle code per l''analisi delle prestazioni di sistemi di telecomunicazione ed elaboarzione dell''informazione;
- Capacità di formulere e risolvere probelmi inrenti la progettazione delle reti e la loro ottimizazione.', 'SISTEMI A CODA (9CFU)
Il corso prede due moduli distinti:
- Reti di Telecomunicazioni (6CFU) mutuato come Telecommunication Networks per il CdLM Ing. Informatica;
- Applicazioni (3CFU).
PROGRAMMI :
MODULO RETI DI TELECOMUNICAZIONI (6 CFU).
Parte I : Teoria delle code.
Sistemi a coda. Formula di Little.
Catene di Markov. Processi di nascita morte: analisi del
transitorio e a regime. Processi di sola nascita.
Processi di Poisson.
Sistemi M/M/1 e M/M/1/K.
Sistemi M/M/S e M/M/S/K. Formule di Erlang B e Erlang C.
Sistemi M/G/1.
Sistemi M/G/1 con tempi di servizio differenziato.
Sistemi M/G/1 con priorità di servizio.
Sistemi G/M/1
Parte II : Reti di code.
Processi di nascita morte a più dimensioni.
Reti in cascata: modello di Burke.
Reti di code di tipo aperto e chiuso: modello di Jackson.
Analisi di reti di comunicazione a memorizzazione ed inoltro.
Analisi di strutture di elaborazione.
Problema dell''allocazione ottima di capacità.
MODULO APPLICAZIONI (3CFU)
Analisi delle reti di comunicazione.
Reti con protocollo di riscontro (ACK) e con protocollo ARQ.
Reti per trasmissione con multiplazione a divisione di tempo asincrona e 
sincrona.
Reti per trasmissione con multiplazione a divisione di frequenza.
Reti con accesso CDMA.
Reti con accesso FDMA.
 Sistemi AMC
Analisi di Tecniche per il controllo della congestione : Sliding Window, Token Bucket, Leaky Bucket.
Software defined Networks;
Applicazioni in contesti reali ( es.: industria 4.0, sistemi complessi, smart city).
Il corso prevede di fornire agli studenti mediante attività integrative le conoscenze di base dei tool Software di uso frequente per la valutazione delle prestazioni di reti di telecomunicazioni.
Sono infine previste attività integrative facoltative o sostitutive per studenti stranieri  in forma di Tutorial in lingua inglese fruibili con la modalità e?learning consentita dal nostro Ateneo.', 'https://www.ing-inm.unifi.it/p-ins2-2017-485257-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (24, '2017/2018', 1, 'VISUAL AND MULTIMEDIA RECOGNITION', 'B024320/B024319', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6/9', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2017-485261-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (25, '2018/2019', 1, 'ADVANCED ALGORITHMS AND GRAPH MINING', 'B027492', 'Ingegneria dell''Informazione', 'INF/01 - INFORMATICA', 'Primo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6', 'Insegnamento mutuato da:
B027492 - ADVANCED ALGORITHMS AND GRAPH MINING
Corso di Laurea Magistrale in INFORMATICA', 'I grafi sono presenti in una gran quantità di applicazioni e lo studio delle loro proprietà consente di capirne la struttura. In questo corso, si studiano algoritmi efficienti per affrontare i seguenti due problemi: l''analisi e il confronto di una collezione di molti grafi relativamente piccoli e l''analisi delle proprietà di un solo grafo molto grande.', 'Dispense.', 'Lo scopo di questo corso è quello di introdurre gli studenti alla progettazione, all''analisi e alla sperimentazione di algoritmi per l''analisi di grafi. Alla fine del corso gli studenti avranno una buona conoscenza dei fondamenti dell''analisi di grafi e saranno in grado di valutare metodi per l''analisi di grafi, di formulare e risolvere problemi legati ai grafi e di analizzare grafi di grandi dimensioni.', 'Lezioni frontali.', 'Il voto finale sarà distribuito tra un esame orale (al momento dell''appello), una presentazione di un articolo e un piccolo progetto pratico, secondo il seguente schema: 25% progetto e relazione, 25% presentazione di un articolo, 50% esame orale.', 'Matching e calcolo di distanze in grafi. Calcolo di distanze basate su trasformazioni. Ricerca di sottostrutture frequenti. Clustering. Reti sociali: preliminari e proprietà. Rilevamento di comunità. Classificazione collettiva. Predizione dei collegamenti. Analisi dell''influenza sociale.', 'https://www.ing-inm.unifi.it/p-ins2-2018-502789-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (26, '2018/2019', 1, 'ADVANCED NUMERICAL ANALYSIS', 'B024332', 'Ingegneria dell''Informazione', 'MAT/08 - ANALISI NUMERICA', 'Primo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6', '*MANCANTE*', 'Metodi di interpolazione ed approssimazione. Interpolazione polinomiale di Hermite e per curve. Le funzioni Splines. Approssimazione di Berstein. La migliore approssimazione ai minimi quadrati. La derivazione numerica. Le formule di quadratura in generale e quelle interpolatorie. MATLAB.', 'L. Gori, Calcolo Numerico IV edizione 2006, Edizioni KAPPA
F. Fontanella, A. Pasquali, Calcolo numerico, Metodi e algoritmi. Vol. 2, 1980, Pitagora Editore
M.L. Lo Cascio, Fondamenti di Analisi Numerica, 2007, McGraw-Hill Bevilacqua , Bini , Capovani, Menchi, Metodi Numerici, 1992, Zanichelli', 'Saper riconoscere e risolvere un problema di natura numerica ed in particolare un problema di approssimazione.
Individuare strategie algoritmiche risolutive.', 'Lezione frontale ed esercitazione Matlab in laboratorio sugli argomenti visti a lezione', 'Esame orale volto anche alla verifca di competenze critiche e trasversali ed alla capacita''di sviluppare algoritmi Matlab per la risoluzione di problemi di analisi numerica', '[1] Approssimazione ed interpolazione:
[1.1] Posizione del problema: classi di funzioni e forma delle possibili approssimanti;
[1.2] Il polinomio interpolante nella forma di Lagrange; Espressione dell''errore;
[1.3] L''errore nel caso dei nodi uniformi ed il suo comportamento asintotico;
[1.4] Stabilita'' nelle formule di interpolazione e la costante di Lebesgue; 
[1.5] I polinomi di Chebyshev; Interpolazione con nodi gli zeri di Chebyshev;
[1.6] Il Teorema di Weierstrass ed in polinomi di Berstein;
[1.7] Polinomi interpolanti di tipo osculatorio ed interpolazione di Hermite; Espressione dell''errore;
[1.8] Le funzioni Splines: definizione, prorieta'', base delle potenze troncate;
[1.9] Spline interpolanti ed approssimanti; Le spline cubiche interpolanti nei nodi (naturali e complete)
1.10] Le B-spline come base dello spazio delle spline e l''algoritmo di De Boor (con particolare attenzione al caso cubico);
[1.1] Il caso parametrico: interpolazione parametrica con parametrizzazione uniforme e della lunghezza dell''arco;
[2] Sistemi lineari rettangolari: il problema lineare dei minimi quadrati min_{x\in \RR^n}||Ax-b||_2 caso m>> n
[2.1] Posizione del problema; Esistenza ed unicita'' della soluzione;
[2.2] Risoluzione mediante il sistema delle equazioni normali A^TAx=A^Tb;
[2.3] Matrici ortogonali e loro proprieta''; le matrici di Hauseholder;
[2.4] Fattorizzazione QR di una matrice utilizzando le matrici di Hauseholder;
[2.5] Risoluzione del problema lineare dei minimi quadrati utilizzando la fattorizzazione QR;
[2.6] La migliore approssimazione ai minimi quadrati trigonometrica ed il caso particolare dell''interpolazione; sviluppo di Fourier: cenni;
[3] Derivazione numerica: idee di base ed alcune semplici formule. Il metodo dei coefficienti indeterminati;
[4] Formule di quadratura (FdQ)
[4.1] Posizione del problema; caso lineare sui nodi x_0,...,x_n: formule del tipo \sum_{i=0}^nw_if(x_i)
[4.2] Grado di precisione g per le FdQ; limitazione superiore del grado di precisione (GdP) g
[4.3] Caso pesi equilimitati: dimostrazione di convergenze delle FdQ all''integrale per n->\infty e studio della stabilita'' della formula;
[4.4] Metodo dei coefficienti indeterminati;
[4.5] FdQ interpolatorie: generalita'' e limite inferiore del grado di precisione (n
[4.5.1] Formule di Newton-Cotes di tipo aperto; grado di precisione ed esempi;
[4.5.2] Formule di Newton-Cotes di tipo chiso; grado di precisione ed esempi;
[4.5.4] Valutazione pratica dell''errore e metodo di estrapolazione di Richardson;
[4.5.5] Formule di quadratura adattative;', 'https://www.ing-inm.unifi.it/p-ins2-2018-502786-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (27, '2018/2019', 1, 'AUTOMATIC CONTROL', 'B028467', 'Ingegneria dell''Informazione', 'ING-INF/04 - AUTOMATICA', 'Primo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6', 'Insegnamento mutuato da:
B014977 - SISTEMI DI CONTROLLO
Corso di Laurea in INGEGNERIA ELETTRONICA E DELLE TELECOMUNICAZIONI
Curricula AUTOMAZIONE', 'Il corso si propone di fornire strumenti matematici per l''analisi e la sintesi  
di sistemi di controllo a retroazione.  
I suoi principali contenuti sono:  
1) Stabilita'' dei sistemi di controllo a retroazione e progetto di controllori  
stabilizzanti.  
2) Tecniche di sintesi diretta. 
3) Limitazioni sulle prestazioni dei sistemi di controllo.
4) Sistemi di controllo a dati campionati.  
5) Sintesi di sistemi di controllo nello spazio di stato.  
6) Controllo ottimo.', 'Materiale didattico disponibile sulla piattaforma Moodle di Ateneo  
Basso, Chisci, Falugi. Fondamenti di Automatica, De Agostini-UTET, 2007.
Bolzern, Scattolini, Schiavoni. Fondamenti di controlli automatici, 3a edizione, Mc Graw-Hill Italia, Milano, 2015.
Doyle, Francis, Tannenbaum. Feedback Control Theory. Maxwell McMillan, 1992.
Goodwin, Graebe, Salgado. Control System Design. Prentice-Hall, 2001. 
Isidori. Sistemi di Controllo: seconda edizione, Vol. I. Siderea, Roma, 1993.', 'Obiettivo del corso è quello di fornire le conoscenze di analisi e sintesi di sistemi di controllo lineari stazionari a retroazione:
- conoscenza delle tecniche per la sintesi di sistemi di controllo ingresso-uscita a tempo continuo: insieme dei controllori stabilizzanti e sue proprietà, sintesi di controllori stabilizzanti in problemi di inseguimento di segnali di riferimento e di reiezione di disturbi, sintesi diretta mediante scelta della funzione di trasferimento ad anello chiuso;
- conoscenza delle limitazioni indotte dalla struttura poli-zeri della funzione di trasferimento dell''impianto sulle prestazioni di sistemi di controllo a tempo continuo: influenza di poli e zeri a destra sulla risposta al gradino e sull''andamento frequenziale delle funzioni di trasferimento del sistema;
- conoscenza delle tecniche per l''implementazione digitale di controllori a tempo continuo: scelta del tempo di campionamento, tecniche di digitalizzazione e tecniche dirette;
- conoscenza di base delle proprietà strutturali di sistemi ingresso-stato-uscita a tempo continuo: raggiungibilità, osservabilità, retroazione statica dallo stato, osservatori asintotici, regolatore;
- conoscenza di base del problema del controllo ottimo di sistemi ingresso-stato-uscita: indice di costo quadratico, ottimizzazione su orizzonte finito e infinito, legge ottima di controllo.
A fine corso lo studente saprà applicare queste conoscenze a problemi di progetto di controllori a tempo continuo per sistemi di controllo lineari stazionari a retroazione e alla loro implementazione digitale.', 'Lezioni ed esercitazioni in aula.', 'La verifica finale consta di una prova scritta e una prova orale in cui attraverso esercizi e domande si verifica la capacità di:
- saper progettare controllori a tempo continuo per problemi di inseguimento di riferimenti e/o reiezione di disturbi mediante sia l''insieme dei controllori stabilizzanti sia la tecnica di sintesi diretta;
- saper implementare in modo digitale i controllori a tempo continuo mediante tecniche di digitalizzazione e tecniche dirette;
- saper valutare le limitazioni sulle prestazioni del sistema di controllo indotte dalla struttura poli-zeri della funzione di trasferimento dell''impianto;
- saper valutare le proprietà di raggiungibilità e osservabilità di sistemi ingresso-stato-uscita anche in relazione al controllo mediante retraozione statica dallo stato e alla stima asintotica dello stato; 
- saper progettare un regolatore per sistemi di controllo ingresso-stato-uscita; 
- saper formulare un problema di controllo ottimo lineare quadratico su orizzonte finito e infinito e caratterizzare la struttura e le proprietà della legge ottima di controllo.', '1. INTRODUZIONE E RICHIAMI
Scopo e linee principali del corso. Richiami sulle proprietà di sistemi lineari stazionari, sull''inseguimento di singoli segnali di riferimento e la reiezione di singoli disturbi (principio del modello interno).
2. STABILITA" DEI SISTEMI DI CONTROLLO A RETROAZIONE E STABILIZZAZIONE 
Stabilità interna: definizione, condizioni e relazioni con il criterio di Nyquist. Caratterizzazione dei controllori stabilizzanti: impianto stabile e impianto instabile; caso del pendolo inverso (con carrello). 
3. TECNICHE DI SINTESI DIRETTA 
Scelta della funzione di trasferimento ad anello chiuso; progetto del controllore sulla base delle specifiche. Procedura sistematica di sintesi diretta e cenni a possibili estensioni.
4. LIMITAZIONI SULLE PRESTAZIONI DEI SISTEMI DI CONTROLLO
Influenza di poli e zeri dell''impianto sulla banda e sulla risposta al gradino del sistema. Teorema di Bode sulla funzione di sensitività S.
5. SISTEMI A DATI CAMPIONATI 
Schemi di sistemi di controllo a dati campionati; campionamento e ricostruzione dei segnali. Discretizzazione di un sistema lineare stazionario a tempo continuo; analisi del comportamento dinamico in trasformata Z. Progetto del controllore digitale: tecniche di digitalizzazione (integrazione, matching), tecniche dirette. 
6. PROBLEMA DEL REGOLATORE 
Richiami sulle rappresentazioni di stato. Osservabilità e raggiungibilità.  Retroazione statica dallo stato e posizionamento degli autovalori (poli). Osservatori asintotici dello stato. Sintesi del regolatore. 
7. CENNI SUL CONTROLLO OTTIMO
Controllo ottimo e programmazione dinamica. Controllo ottimo Lineare Quadratico (LQ) su orizzonte finito per sistemi a tempo discreto. Regolatore LQ su orizzonte infinito per sistemi a tempo discreto. Regolatore LQ su orizzonte infinito per sistemi a tempo continuo.', 'https://www.ing-inm.unifi.it/p-ins2-2018-502788-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (53, '2018/2019', 1, 'TELECOMMUNICATION NETWORKS', 'B024339', 'Ingegneria dell''Informazione', 'ING-INF/03 - TELECOMUNICAZIONI', 'Secondo Anno - ', 'dal  al ', '6', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502804-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (54, '2018/2019', 1, 'VISUAL AND MULTIMEDIA RECOGNITION', 'B024320/B024319', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - ', 'dal  al ', '6/9', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502802-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (28, '2018/2019', 1, 'COMPUTER GRAPHICS AND 3D', 'B024312/B024311', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Primo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6/9', '*MANCANTE*', '1. Computer graphics
The computer graphics pipeline will be presented and analyzed, by focusing on the aspects of modeling, animation and rendering of a 3D scene. Practical examples will be given using OpenGL and Matlab code.
2. 3D acquisition and processing 
We will focus on the acquisition and processing of 3D real data. Algorithms and methods will be addressed for concrete applications, like 3D retrieval, 3D recognition, 3D biometrics, etc.', '[1] Steven J. Gortler, "Foundations of 3D Computer  Graphics," The MIT Press, 2012
[2] John F. Hughes et al., "Computer  Graphics,   Principles and Practice," Wiley and Sons, Third   Edition, 2014
[3] Graham Sellers et al. "OpenGL SUPERBIBLE", Addison Wesley, Seventh Edition, 2015', 'L''obiettivo del corso è duplice: 
- da un lato, verrà presentata una completa introduzione a concetti di base ed avanzati della grafica computazionale, presentando le più comuni tecniche per la modellazione 3D, l''animazione ed il rendering;
- dall''altro, saranno considerati gli aspetti specifici relativi alla acquisizione ed elaborazione di dati 3D reali acquisiti con scanner sia a bassa che ad alta-risoluzione, con l''obiettivo di presentare anche applicazioni pratiche, come riconoscimento e ricerca di oggetti 3D, biometria da dati 3D, etc. Sarà anche data una breve introduzione ai dispositivi di stampa 3D.
La teoria ed i concetti presentati nel corso saranno applicati utilizzando codice OpenGL e Matlab.', 'Lezione frontale in aula con uso di slide ed alla lavagna. Esercitazioni in laboratorio (programmazione C++ e Javascript con API OpenGL e Matlab)', 'La valutazione è basata su:
- un elaborato di programmazione su argomenti di computer graphics o 3D da eseguire individualmente o in coppia. L''elaborato ha l''obiettivo di valutare la comprensione delle tecniche e dei metodi principali di Comptuer Graphics e di elaborazione di dati 3D introdotti durante il corso e la capacità di tradurre gli stessi in una idea e realizzazione progettuale.
- esame orale su un sottoinsieme degli argomenti del corso. La prova orale mira a valutare la comprensione delle tecniche di computer graphics presentate nel corso e la capacità di applicarle a casi concreti. 
In alternativa è possibile sostenere la sola prova orale su tutti gli argomenti del corso.', '1. Computer graphics
In the first part of the course, the computer graphics pipeline will be presented and analyzed, by focusing on the aspects of modeling, animation and rendering of a 3D scene. Some hints of the 2D case will be also given. Practical examples on the different subjects will be given using OpenGL and Matlab code. The same programming frameworks will be used for experimental activities (laboratory work and assignments).
Modeling
Essential mathematics and the geometry of 2D-space and 3D-space
- Curves and surfaces: Bézier curves and splines
A simple way to describe shape in 2D and 3D
- "Meshes" in 2D: polylines
- Meshes in 3D: manifold and non-manifold meshes, basic mesh operations
Curve Properties & Conversion, Surface Representations
Coordinates and Transformations: Homogeneous coordinates, Perspective
Hierarchical models: Hierarchical grouping of objects (scene graph), Hierarchical Modeling in OpenGL
Animation
Articulated models: Joints and bones, skeleton hierarchy, forward kinematics, inverse kinematics
Color: Spectra, Cones and spectral response, Color blindness and metamers, Color matching, Color spaces
Character Animation: Animate simple "skeleton", Attach "skin" to skeleton
Basics of Computer Animation: Keyframing, Procedural, Physically-based 
Animation Controls 
Character Animation using skinning/enveloping
Collision detection and response: Point-object and object-object detection, Only point-object response
Rendering
Ray Casting
Rasterization
Ray Tracing: Shade (interaction of light and material), Secondary rays (shadows, reflection, refraction)
Textures and Shading
Texture Mapping & Shaders: Sampling & Antialiasing, Shadows, Global illumination
OpenGL
Introduction to the Open Graphics Library: Industry standard graphics library used to produce real-time 2D and 3D graphics
2. 3D acquisition and processing 
In the second part of the course, we will focus on the acquisition and processing of 3D real data (both with low and high resolution). Algorithms and methods will be addressed for concrete applications, like 3D retrieval, 3D recognition, 3D biometrics, etc. 
3D acquisition
High resolution 3D scanners technology, Low resolution 3D cameras (Kinect)
Algorithms on the mesh
Tangent, Normal, Curvature (mean, Gaussian, principal)
Geodesic computation on the mesh: Dijkstra, Fast marching algorithm
3D descriptors on the mesh: shape-index, shape-context, mesh-SIFT, mesh-HOG, mesh-LBP
Laplace-Beltrami operator
Applications
3D objects retrieval, 3D recognition (static and dynamic), 3D biometrics
Modern graphics hardware
Graphics Processing Unit (GPU) for real-time 3D computer graphics
GPU programming with CUDA
3D printing
Basic technologies and applications', 'https://www.ing-inm.unifi.it/p-ins2-2018-502780-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (29, '2018/2019', 1, 'DATA AND DOCUMENT MINING', 'B024276/B024275', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Primo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6/9', '*MANCANTE*', 'Data Mining
Document Engineering
Document Image Analysis
Information Retrieval', '(I) C.D. Manning, P. Raghavan, P. Raghavan Introduction to Information Retrieval, Cambridge University Press - 2008
(I) A. Rajaraman, J. D. Ullman, Mining of Massive Datasets, 2011
(L) D. Doermann, K. Tombre (Eds.) Handbook of Document Image Processing and Recognition, 2014 (L)
Note:
(L) : Book available in the Engineering library
(I) : Book available in Internet (authors'' version)', 'Obiettivo del corso è quello di fornire le conoscenze e capacità necessarie a progettare e sviluppare sistemi che permettano di estrarre conoscenza da grandi quantità di dati con particolar riferimento ad applicazioni nell''ambito di sistemi di analisi di immagini di documenti.
- Conoscenza delle tecniche di base del Data Mining che consentono di modellare grandi 
quantità di dati ed estrarne informazione utile.
- Conoscenza delle problematiche relative all''estrazione di informazione ed indicizzazioni di documenti sia testuali che non testuali. 
- Conoscenza dei principali modelli e algoritmi in Information Retrieval 
- Conoscenza  delle principali tecniche per l''estrazione di informazioni da documenti digitalizzati e quindi acquisite prevalentemente sotto forma di immagini.', 'Lezioni frontali, esercitazioni in classe, svolgimento assistito
di elaborati.', 'Durante il corso è prevista l''analisi di un articolo scientifico e la sua presentazione ai colleghi durante le lezioni
La verifica finale consta di un elaborato e di una prova orale.
L''elaborato può essere o individuale e basato sullo studio di alcuni articoli scientifici o in gruppo e finalizzato all''implementazione al test e all''analisi di una semplice applicazione nell''ambito degli argomenti del corso.
Nel complesso la verifica deve permettere allo studente di mostrare le capacità di:
  - Saper analizzare un problema pratico e progettare una sua soluzione
  - Saper applicare le principali tecniche descritte durante il corso tramite implementazione di un modulo software o tramite analisi teorica
  - Saper interagire con colleghi per portare avanti lo svolgimento del progetto
  - Saper descrivere in modo accurato in forma scritta il lavoro svolto e fornire una appropriata analisi dei risultati
  - Saper descrivere metodi ed algoritmi degli argomenti trattati nel corso', 'Data Mining
Datawarehouse. Hardware. Disk Organization. Access times
Distributed file system and the new software stack
Map Reduce, Word count, Matrix-Vector and Matrix Multiplication with Map Reduce
The market-basket model . Association rules. Implementation details. Algorithms for computing frequent item-sets and Association Rules.
Improving Apriori: Hash-based filtering. Bloom filters. PCY algorithm, Random sampling, SON algorithm, Apriori with MapReduce-
Finding similar items. Curse of dimensionality.  Distance measures.
Document similarity, shingling, min-hashing
Locality sensitive hashing (LSH)
Families of hash functions. LSH for cosine distance. LSH for Euclidean distance.
Curse of dimensionality.  Distance measures.
Clustering, Hierarchical clustering,  k-means clustering. SOM clustering
BFR algorithm, CURE algorithm. Dimensionality reduction
Document Image Analysis and Recognition
DIAR: preprocessing
Object segmentation
Layout analysis : RLSA, Docstrum, Area Voronoi diagram, XY tree, MXY tree, Reading order detection, classification in layout analysis, page classification/retrieval.
Layout analysis :  XY tree, MXY tree, Reading order detection, classification in layout analysis, page classification/retrieval. OCR.
Artificial Neural Networks. Perceptron, Backpropagation
Convolutional neural networks
Document Image Retrieval
Information Retrieval
Introduction to Information Retrieval. Boolean Retrieval
Term vocabulary and postings lists, Inverted files
Vector Space Model
Tokenization, stop-word removal, stemming
Index construction
Index compression
Processing boolean queries
Computing Scores in complete search system - Efficient scoring and ranking, Components of an information retrieval system. Vector space scoring and query operator interaction.
Phrase queries
Wildcard queries.
Orthographic correction.
Performance Evaluation in IR systems
Web mining', 'https://www.ing-inm.unifi.it/p-ins2-2018-502779-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (30, '2018/2019', 1, 'DATA SECURITY AND PRIVACY', 'B027204', 'Ingegneria dell''Informazione', 'INF/01 - INFORMATICA', 'Primo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6', 'Insegnamento mutuato da:
B027502 - DATA SECURITY AND PRIVACY
Corso di Laurea Magistrale in INFORMATICA
Curricula DATA SCIENCE', 'Network security. Crittografia a chiave condivisa. Cifrari perfetti secondo Shannon, One-Time-Pad, unicity distance. Cifrari di Feistel. Crittografia a chiave pubblica. Elementi di aritmetica modulare. I cifrari RSA e El Gamal, il protocollo di Diffie-Hellman. Funzioni hash one-way crittografiche. Autenticazione e firma digitale.
Elementi di Teoria dell''Informazione e codici.
Privacy. k-anonymity e attacchi background knowledge.', '1) Michele Boreale. Note per il corso di Codici e Sicurezza.
Disponibile in rete.
2) Articoli e materiale forniti dal docente.', 'Conoscenze - Il corso mira a fornire agli studenti una comprensione approfondita dei principi scientifici alla base della trasmissione efficiente, affidabile e sicura dei dati.
Competenze - 
Alla fine del corso, lo studente dovrebbe saper padroneggiare e applicare i principi teorici fondamentali (matematici, crittografici ed algoritmici) alla base della protezione dei dati e della Privacy.
Capacità – Alla fine del corso, lo studente avrà acquisito  la capacità di costruire modelli ad alto livello, ma rigorosi, dei sistemi di comunicazione, ed analizzare le criticità dal punto di vista della Sicurezza.', 'Lezioni frontali.', 'L''esame consiste di due parti:
1. Svolgimento di un progetto, che può essere o di tipo programmativo o di approfondimento teorico, centrato su argomenti inerenti i codici e la sicurezza dei dati (*);
2. prova orale sugli argomenti del corso e sul progetto svolto.
(*) esempi di svolgimento di tali progetti sono resi disponibili attraverso la piattaforma di e-learning.', 'Network security. Crittografia a chiave condivisa. Cifrari perfetti secondo Shannon, One-Time-Pad, unicity distance. Cifrari di Feistel. Crittografia a chiave pubblica. Elementi di aritmetica modulare. I cifrari RSA e El Gamal, il protocollo di Diffie-Hellman. Funzioni hash one-way crittografiche. Autenticazione e firma digitale
Elementi di Teoria dell''Informazione. Codici di compressione: 1^ Shannon, codici Huffman. Canali con rumore, capacità e codici rilevatori e correttori. 
Privacy: k-anonimity, l-diversity, t-closeness. Background information. Differential Privacy. (*)
Approfondimenti di crittografia: il cifrario AES; prova di correttezza del test di Miller-Rabin; key equivocation e unicity distance; schemi undeniable di firma digitale. (*)
(*) Non incluso nei corsi mutuati da 6 CFU.', 'https://www.ing-inm.unifi.it/p-ins2-2018-502783-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (31, '2018/2019', 1, 'DATA WAREHOUSING', 'B027206', 'Ingegneria dell''Informazione', 'INF/01 - INFORMATICA', 'Primo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6', 'Insegnamento mutuato da:
B014443 - DATA WAREHOUSING
Corso di Laurea Magistrale in INFORMATICA
Curricula DATA SCIENCE', 'Il corso si propone di presentare allo studente gli strumenti e le metodologie per l''analisi dei dati a supporto del processo decisionale. Modellazioni concettuali e modelli semantici, Business Intelligence e sistemi di supporto alle decisioni. Sistemi informativi per metadati, Sistemi informativi per la rappresentazione dei processi, l''approccio relazione per la rappresentazione dello spazio e del tempo  Applicazioni OLTP e applicazioni OLAP.', 'Matteo Golfarelli, Stefano Rizzi Data Warehouse - Teoria e pratica della progettazione 3/ed 
Mc-Graw-Hill
Materiale predisposto dai docenti', 'Acquisire capacità e competenze di analisi e di rappresentazione dei problemi. 
Competenze di gestione di sistemi informativi a supporto delle decisioni,  conoscenza delle tecniche di progettazione e gestione di basi dati storici e di data warehouse', 'Lezioni frontali', 'Discussione di una breve relazione 
Verifica orale', '• dal linguaggio ai modelli informativi
• temporal reasoning
• la dimensione spaziale nell''ambito dei sistemi informativi a supporto delle decisioni e della conoscenza
• il ruolo delle ontologie nella predisposizione dei sistemi informativi a supporto delle decisioni
• modelli informativi per la rappresentazione dei processi
• oltre il relazionale- modelli concettuali per dati aggregati
• modellazione di sistemi informativi a supporto del monitoraggio delle politiche 
• Business Intelligence e sistemi di supporto alle decisioni.
• Applicazioni OLTP e applicazioni OLAP.
• Architetture e progettazione di un Data Mart
• Strumenti ETL
• Modello multidimensionale.
• Strumenti di reportistica avanzata, navigazione Olap
• Panoramica delle tecniche di Data Mining.
introduzione al semantic web 
breve Presentazione di owl e sparql', 'https://www.ing-inm.unifi.it/p-ins2-2018-502778-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (32, '2018/2019', 1, 'IMAGE AND VIDEO ANALYSIS', 'B024271/B024269', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Primo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6/9', '*MANCANTE*', '.', '- Computer Vision: Models, Learning, and Inference
Simon J. D. Prince
University College London
- Computer and Machine Vision: Theory, Algorithms, Practicalities
E. R. Davies
Academic Press
- Digital Image Processing
R.C. Gonzalez, R.E. Woods
Ed. Pearson, Prentice Hall
- Materiale disponibile sul sito web del docente www.dsi.unifi.it/pala/
- Altro materiale
www.icaen.uiowa.edu/~dip/LECTURE/contents.html', 'Obiettivo del corso è quello di fornire le conoscenze e competenze
necessarie ad effettuare operazioni di elaborazione ed analisi di immagini e video.  
- Conoscenza dei modelli per la descrizione di caratteristiche globali e locali delle immagini in termini di colore e tessitura
- Conoscenza di modelli per la rappresentazione multiscala del contenuto di immagini
- Conoscenza dei modelli per la sogliatura e segmentazione di immagini in base a caratteristiche locali
- Conoscenza di modelli per la descrizione della dinamica in un video
- Capacità di applicare le conoscenze acquisite per il progetto di moduli software per l''analisi del contenuto di immagini e video.', 'Lezioni in aula ed esercitazioni in laboratorio con Matlab', 'L''esame si svolge attraverso una prova orale, eventualmente affiancata dallo sviluppo di un elaborato. Nella prova viene verificata la capacità comunicativa del candidato e la conoscenza dei modelli di descrizione di immagini e video e la capacità di formalizzare in forma algoritmica o con pseudocodice la struttura di un modulo di analisi di immagini/video con riferimento a problemi di segmentazione di immagine e stima del flusso ottico.', 'Proprietà metriche, distanza di Hausdorff, distanza di Mahalanobis, proprietà topologiche.
Trasformazioni puntuali: istogramma, espansione di scala, equalizzazione di immagine: modelli globali e locali
Trasformazioni position dependent; Local histogram equalization ; trasformazioni geometriche: applicazione e stima;  image morphing
Operatori locali: introduzione; Smoothing: media, gaussiano; bilateral filter, filtro mediano; Correlazione e template matching; 
Concetto di edge; edge e derivate; operatori del primo ordine; maschere di roberts, prewit, sobel; operatori del secondo ordine; Laplaciano, LoG, DoG; Filtro di canny: principi, soppressione non massimale, isteresi; Contronto risultati edge su immagine con rumore; edge detection con modelli parametrici; estrazione edge su immagini a colori; 
Corner detection: modello di Harris
Rappresentazione multiscala e scale-space: motivazioni e definizione del modello, N-jet ed invarianti; Anisotropic diffusion models;
Invarianti di colore; 
Descrizione tessiture: introduzione, motivazione e contesti applicativi; matrici co-occorrenza, matrice di covarianza; Tamura, Local Binary Patterns, Fourier Power Spectrum, Region covariance, Gabor, Momenti; 
La Gestalt ed i principi di raggruppamento; Sogliatura di immagini con il metodo di Otsu;  Sogliatura immagini rumorose, sogliatura con media mobile; 
Binary mathematical morphology: erode and dilate;  open, close, smooth, hit-or-miss, gradient, hole filling; binary reconstruction by dilation and erosion, open by reconstruction, ultimate erosion, distance transform. 
Lattice morphology: erosion, dilation, open, close, morphological gradient, top-hat, bottom-hat, contrast enhancement, grayscale reconstruction, opening by reconstruction; Granulometry with MM; 
Segmentazione di immagini attraverso clustering;  Clustering gerarchico, K-means
Gaussian Mixtures, Algoritmo EM; Mean Shift; 
Segmentazione basata su grafi: Normalized Cuts; 
Edge-based segmentation: Hough rette, rette con parametrizzazione theta; Hough cerchi e generalizzata; Watersheds
Principio di formazione del moto sul piano immagine. Applicazioni ed esempi pratici di analisi del moto.  Relazione tra 3D motion field e 2D motion field.
Flusso ottico. Definizione e relazione con 2D motion field. Casi particolari e degeneri. Distinzione tra metodi densi e metodi sparsi. Stima del flusso ottico tramite metodi differenziali: algoritmo Horn&Schunk.
Stima del flusso ottico tramite il metodo di Lucas e Kanade. Stima del flusso con modello di moto globale affine: l''algoritmo KLT. Stima in caso di displacement elevato: algoritmo KLT piramidale, metodo di Brox e Malik basato su Region Matching.
Segmentazione del moto. Casi possibili (telecamera fissa, brandeggiabile, in movimento). Segmentazione background/foreground. Metodi dinamici di modellazione del background basati su modelli statistici. Algoritmo Mixture of Gaussian.', 'https://www.ing-inm.unifi.it/p-ins2-2018-502785-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (33, '2018/2019', 1, 'INDUSTRIAL AUTOMATION', 'B028468', 'Ingegneria dell''Informazione', 'ING-INF/04 - AUTOMATICA', 'Primo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6', 'Insegnamento mutuato da:
B019037 - AUTOMAZIONE INDUSTRIALE
Corso di Laurea Magistrale in INGEGNERIA ELETTRICA E DELL''AUTOMAZIONE', 'Il corso offre una panoramica dei sistemi di produzione industriale e presenta un''approfondita analisi degli aspetti metodologici e tecnologici relativi ai controllori industriali PID e PLC.', 'Testo di riferimento:
[BGP11] Claudio Bonivento, Luca Gentili, and Andrea Paoli.
Sistemi di automazione industriale. Architetture e controllo.
McGraw-Hill, Milano, Italia, 2011.
ISBN 978 88 386 6693–3.
Altri testi suggeriti:
[CB04] Pasquale Chiacchio and Francesco Basile.
Tecnologie informatiche per l''automazione.
McGraw-Hill, Milano, Italia, 2a edition, 2004.
ISBN 88 386 6147–2.
[MFR07] GianAntonio Magnani, Gianni Ferretti, and Paolo Rocco.
Tecnologie dei sistemi di controllo.
McGraw-Hill, Milano, Italia, 2007.
ISBN 978 88 386 7275–0.
Testi per approfondimenti:
[But95] Giorgio C. Buttazzo.
Sistemi in tempo reale.
Pitagora Editrice, Italia, 1995.
ISBN 88 371 1640–3.
[Fal09] Alessandro Falaschi.
Elementi di Trasmissione dei Segnali e Sistemi di Telecomunicazione.
Edizioni ilmiolibro, Italia, v0.99 edi- tion, 2009.
ISBN 978 88 548 2798–1.
Gratuitamente scaricabile all''indirizzo: http://infocom.uniroma1.it/alef/wiki/Didattica/TrasmissioneDeiSegnaliESistemiDiTelecomunicazione .
[FLV00] Augusto Ferrante, Antonio Lepschy, and Umberto Viaro.
Introduzione ai controlli automatici.
UTET Libreria, Italia, 2000.
ISBN 978 88 251 7335–2.', 'Obiettivo del corso è istruire lo studente sulle problematiche, le soluzioni metodologiche e le tecnologie che sono comunemente usate nella produzione industriale. Obiettivi nel dettaglio:
- conoscenza delle principali architetture e modelli di riferimento per un Sistema di Produzione Industriale
- conoscenza delle tecnologie e degli strumenti più usati per la realizzazione di sistemi di controllo industriali;
- conoscenza delle principali normative di riferimento per sistemi di controllo e reti di comunicazione in ambito industriale;
- conoscenza delle principali problematiche legate ai Controllori di Campo, ai Controllori di Procedura e al loro interfacciamento con i processi produttivi;
- le soluzioni teoriche su cui si basano le tecnologie dei Controllori di Campo, dei Controllori di Procedura, delle Reti di Campo e delle Reti per il Controllo;
- saper progettare uno Schema di Controllo di Campo;
- saper tarare un controllore PID;
- saper strutturare un Sistema di Controllo di Procedura;
- saper programmare un controllore PLC mediante linguaggi Ladder Diagram e Sequential Functional Chart.', 'Lezioni in aula.', 'L''esame finale consiste in una prova orale rivolta a verificare:
- la conoscenza dei modelli, delle architetture di riferimento per i Sistemi di Produzione Industriale;
- la conoscenza delle tecnologie e degli strumenti più comunemente usati nella realizzazione di sistemi di controllo industriali;
- la conoscenza delle normative di riferimento per sistemi di controllo e reti di comunicazione industriali;
- la conoscenza delle basi teoriche dei Controllori di Campo, dei Controllori di Procedura, delle Reti di Campo e delle Reti per il Controllo;
- la conoscenza delle problematiche legate ai Controllori di Campo, ai Controllori di Procedura e al loro interfacciamento con i processi produttivi;
- la conoscenza dei metodi di taratura dei controllori PID;
- la conoscenza dei linguaggi di programmazione Ladder Diagram e Sequential Functional Chart.
- la capacità di saper progettare uno Schema di Controllo di Campo;
- la capacità di saper tarare un controllore PID;
- la capacità di saper strutturare un Sistema di Controllo di Procedura;
- la capacità di saper programmare un controllore PLC mediante linguaggi Ladder Diagram e Sequential Functional Chart.
Una volta per ogni sessione di appelli (estiva e invernale) è data la possibilità di effettuare una prova scritta di esonero dall''esame orale, rivolta a verificare le conoscenze e le capacità sovra indicate attraverso domande a risposta sintetica ed esercizi pratici.', 'L''automazione industriale. Il processo produttivo e l''automatica [BGP11, c. 1], [CB04, c. 12]: processo automatizzato, sistema di produzione, automazione dell''impianto di produzione, automazione del sistema di supporto. L''architettura "Computer Integrated Manufactoring" (CIM) [BGP11, c. 1], [CB04, c. 7, 10, 11]: modello gerarchico piramidale, infrastruttura di comunicazione aziendale, standard ANSI/ISA-88.01-1995 "Batch Control", architetture elettroniche per il controllo.
Il controllore PID. Progetto di controllori di campo [BGP11, c. 4], [FLV00, c. 1, 2, 4, 11]: problemi del controllo, segnali di riferimento, specifiche del controllo. Controllore PID ideale [BGP11, c. 4], [FLV00, c. 11], [MFR07, c. 7]: rappresentazione matematica, parametri industriali, tecniche di taratura, schemi di controllo multi-PID, formulazione ISA, schema PI-D con anello interno. Controllore PID reale [BGP11, c. 4], [FLV00, c. 11], [MFR07, c. 7]: accorgimenti realizzativi, fenomeno "wind-up" della saturazione degli attuatori, back calculation and tracking. Schema tecnologico del controllo di campo [BGP11, c. 4], [Fal09, cc. 3, 4, 7], [MFR07, c. 4]: adattamento in potenza, trasduzione, conversione, codifica, campionamento, teorema di Shannon, effetto aliasing, effetti del campionamento non ideale, decodifica, interpolazione continua. Dispositivi di controllo tempo discreti [But95, c. 7], [BGP11, c. 4]: progetto per discretizzazione, progetto per mappattura dei poli e degli zeri, PID digitale.
Il controllore PLC. Cenni storici sui controllori logici sequenziali programmabili [BGP11, c. 6], [CB04, c. 8]. Normative IEC 61131-1 e IEC 61131-2 [BGP11, c. 6], [CB04, c. 8]: definizioni e generalità, architettura hardware, moduli, architettura software, sistema operativo, modalità operative. Modulo processore - Caratteristiche dei dispositivi real-time [BGP11, c. 2], [But95, cc. 1-12]: programmazione concorrente, scheduler, scheduling vincolato, starvation, deadlock, sistemi hard e soft real-time, sistemi operativi real-time, funzionamento a copia massiva di ingressi e uscite, rilevazione delle attivazioni. Moduli I/O - Tecnologie per la connettività [BGP11, c. 3], [CB04, c. 11, 15], [MFR07, c. 10]: classificazioni delle reti di calcolatori, mezzi fisici di trasmissione, codifica dei segnali, raccomandazione ISO/IEC 7498-1 "modello ISO-OSI", normativa IEC 61158 "Fieldbus", normativa ISO 11898 "Controller Area Network" (CAN). Normativa IEC 61131-3 [BGP11, c. 6], [CB04, c. 8]: linguaggi di programmazione standard di un PLC, programmazione in linguaggio Ladder Diagram, programmazione in linguaggio Sequential Functional Chart.
Introduzione ai sistemi di controllo con campionamento aperiodico. Approccio guidato dagli eventi (event-triggered control) e autoguidato (self-triggered control): tecniche a emulazione, tecniche input-output (funzionali energetici, dissipatività), tecniche input-to-state, tempo minimo di scansione, effetto Zeno.', 'https://www.ing-inm.unifi.it/p-ins2-2018-502775-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (34, '2018/2019', 1, 'INFORMATION THEORY', 'B026371', 'Ingegneria dell''Informazione', 'ING-INF/03 - TELECOMUNICAZIONI', 'Primo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6', 'Insegnamento mutuato da:
B019012 - TEORIA DELL''INFORMAZIONE
Corso di Laurea Magistrale in INGEGNERIA DELLE TELECOMUNICAZIONI', 'Il corso affronta il problema di trasmettere efficacemente e fedelmente un messaggio. 
Introduce l''informazione come qualcosa di definito e misurabile e fornisce risposta a due quesiti fondamentali per qualsiasi sistema di informazione, ovvero quale è il massimo livello di compressione e quale è la massima velocità di trasmissione dei dati. 
Il corso fornisce poi elementi di codifica sia di sorgente che di canale, utilizzate per avvicinarsi ai limiti teorici  definiti in precedenza.', 'Il materiale didattico dell''intero corso è disponibile sulla piattaforma Moodle.
Come ulteriori testi di consultazione per eventuali approfondimenti:
T. M. Cover, J. A. Thomas: Elements of Information Theory. John Wiley & Sons, New York,  2nd ed 2006
N. Abramson: Information Theory and Coding. McGraw-Hill, New York, 1963.
S. Lin, D. J. Costello Jr.: Error Control Coding: Fundamentals and Applications. Prentice-Hall, 1983.
K. Sayood: Introduction to data compression, Elsevier 4a ed., 2012
S. Benedetto, E. Biglieri, V. Castellani: Digital Transmission Theory, Prentice Hall, 1988.
J. G. Proakis: Digital Communications. McGraw-Hill, 4a Ed., 2001.
A. Papoulis, S.U. Pillai, Probability, Random Variables, and Stochastic Processes, 4th ed., McGraw-Hill, 2002.', 'Il corso ha lo scopo di fornire le conoscenze di base per la rappresentazione in forma compatta dell''informazione e la trasmissione affidabile dell''informazione su un canale di comunicazione con rumore, in particolare:
- Conoscenze dei concetti di base sulla rappresentazione dell''informazione
- Conoscenza degli elementi fondamentali della codifica di sorgente e tecniche di codifica di sorgente
- Conoscenza delle problematiche e caratterizzazione della trasmissione affidabile su canali rumorosi
- Conoscenza sui principi della codifica di canale e tecniche di codifica di canale.
Al termine del corso, lo studente avrà acquisito la capacità di: 
- comprendere i principi alla base degli standard correnti per la compressione di dati, audio, immagini e video 
- saper applicare tecniche di compressione di sorgente
- comprendere i principi alla base delle tecniche di protezione di un segnale trasmesso su un canale con rumore
- saper applicare tecniche di codifica di canale', 'l corso sarà costituito da:
Lezioni frontali svolte con l''ausilio di dispense fornite dal docente (messe a disposizione sulla piattaforma Moodle)
Seminari finali su nuovi ambiti applicativi della Teoria dell''Informazione', 'La verifica finale consta di una prova orale durante la quale verranno fatte domande teoriche e svolti esercizi.
Le domande teoriche hanno lo scopo di verificare
- la comprensione del concetto di informazione
- la comprensione della teoria che sta alla base della compressione e della codifica di canale
- la comprensione dei meccanismi e dei principi che permettono di costruire sia un codice di sorgente che di canale
- la comprensione delle caratteristiche dei codici di sorgente e di canale e le metriche di valutazione delle prestazioni
Gli esercizi hanno lo scopo di verificare
- la capacità di applicare i concetti teorici
- la capacità di applicare le tecniche di codifica analizzate', 'PARTE I –  INTRODUZIONE - Misura dell''informazione ed Entropia. Entropia di sorgenti discrete, sorgenti senza memoria e con memoria, sorgenti continue. 
PARTE II – COMPRESSIONE DI SORGENTE- Introduzione alla codifica di sorgente, classificazione dei codici. Disuguaglianze di Kraft e di McMillan. Lunghezza media di un codice. Primo teorema di Shannon.  Codifica di Huffman. Codifica aritmetica. Codifica di Lempel-Ziv, Codifica Run-Length
PARTE III- RATE DISTORTION THEORY- Quantizzazione, distorsione e misura della distorsione. Teoria della Rate-Distortion (cenni): significato della curva di rate-distortion di una sorgente. Funzione di rate distortion per variabili Gaussiane
PARTE IV - CAPACITA'' DI CANALE - Modelli di canale per la trasmissione di informazione. Equivocazione di canale. Capacità di canale. Regole di decisione e probabilità di errore. Disuguaglianza di Fano. Secondo teorema di Shannon, sulla trasmissione affidabile di informazione su canali rumorosi. Capacità di un canale Gaussiano. Limite di Shannon e regione di comunicazione affidabile. 
PARTE V - CODIFICA DI CANALE - Codifica a controllo d''errore. Rivelazione e correzione di errori. Codici blocco. Codici lineari. Decodifica hard di codici lineari. Codici ciclici. Codici BCH. Codici Reed-Solomon. Codici concatenati. Tecniche di interleaving. Codici convoluzionali. Decodifica dei codici convoluzionali: algoritmo di Viterbi. Decodifica soft. Guadagno di un codice di canale.
Seminari (i) su Turbo Codifica e LDPC (ii) su physical layer security', 'https://www.ing-inm.unifi.it/p-ins2-2018-502768-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (35, '2018/2019', 1, 'LABORATORY OF AUTOMATIC CONTROL', 'B028466', 'Ingegneria dell''Informazione', 'ING-INF/04 - AUTOMATICA', 'Primo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6', 'Insegnamento mutuato da:
B019036 - LABORATORIO DI AUTOMATICA
Corso di Laurea Magistrale in INGEGNERIA ELETTRICA E DELL''AUTOMAZIONE', 'Applicazione e confronto di diverse tecniche di progetto di sistemi di controllo automatico su alcuni processi fisici reali presenti in laboratorio. Saranno inoltre trattati temi relativi ai sistemi di controllo in tempo reale e alla gestione dei processi attraverso internet. Software impiegati: MATLAB/SIMULINK.', 'Bonivento, Gentili, Paoli, "Sistemi di Automazione Industriale", McGraw-Hill, 2010.', 'L''obiettivo del corso è quello di sperimentare le tecniche di base per la sintesi di controllori automatici e sistemi di automazione su alcuni dei più comuni processi di cui sono equipaggiati i laboratori didattici di controlli automatici.
Conoscenza e comprensione
Gli obiettivi di apprendimento attesi riguarderanno principalmente la programmazione avanzata di strumenti CAD (Matlab/Simulink) per il progetto di sistemi di controllo a retroazione.
Capacità di applicare conoscenza e comprensione
Lo studente dovrà essere in grado di applicare la conoscenza acquisita nei corsi di automatica di base (I livello) per il progetto di anelli di controllo su processi fisici di laboratorio.
Ulteriori risultati di apprendimento attesi:
Autonomia di giudizio
Nel progetto da sviluppare la definizione delle specifiche del problema non sono complete e lasciano vari gradi di libertà allo studente che deve essere in grado di fare delle scelte personali.
Abilità comunicative
Le abilità comunicative vengono esercitate e valutate attraverso lo specifico svolgimento di una relazione scritta riguardante l''esperienza di laboratorio e dalla sua esposizione di fronte agli altri studenti del corso.', 'Lezioni in Aula. 
Esperimenti di gruppo in Laboratorio da concordare con il docente.', 'La verifica dell''apprendimento avviene attraverso la stesura di un elaborato con relativa esposizione orale.
In particolare, i criteri di valutazione sono egualmente suddivisi fra:
1) capacità di presentare il progetto sviluppato in forma verbale, mediante la proiezione di slides ed eventualmente anche attraverso la composizione di un video che descriva le varie fasi del progetto;
2) capacità di presentare il lavoro in forma scritta attraverso la redazione di un rapporto tecnico che descriva coerentemente il problema affrontato, le soluzioni tecniche adottate e i risultati ottenuti;
3) capacità di lavorare in gruppo attraverso un opportuno coordinamento ed una chiara divisione di compiti da cui si evinca il contributo dei singoli. Capacità di problem solving. Capacità di rispettare le scadenze per la consegna del progetto definitivo.', '- Introduzione ai principi di funzionamento dei sistemi operativi real-time;
- Controllo remoto mediante interfaccia web; 
- Utilizzo software MATLAB e SIMULINK;
- Pendolo inverso di Furuta; 
- Pendolo inverso lineare; 
- Levitatore magnetico; 
- Ball & beam; 
- Elicottero vincolato; 
- Processo termico;
- Lego Mindstorms.', 'https://www.ing-inm.unifi.it/p-ins2-2018-502774-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (36, '2018/2019', 1, 'NAVIGATION AND ESTIMATION OF MOBILE ROBOTS', 'B028469', 'Ingegneria dell''Informazione', 'ING-INF/04 - AUTOMATICA', 'Primo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6', 'Insegnamento mutuato da:
B010314 - STIMA E IDENTIFICAZIONE
Corso di Laurea Magistrale in INGEGNERIA ELETTRICA E DELL''AUTOMAZIONE
Curricula AUTOMAZIONE E ROBOTICA', 'Il corso si propone di fornire strumenti statistici per 
l''analisi e  l''elaborazione dei dati con particolare riferimento a problemi 
di stima di parametri, segnali e modelli di sistemi dinamici.
Il programma consiste di 4 parti:
1. SEGNALI E SISTEMI DINAMICI STOCASTICI
2. TEORIA DELLA STIMA
3. FILTRAGGIO RICORSIVO E SUE APPLICAZIONI
4. IDENTIFICAZIONE DI MODELLI', 'Appunti dalle lezioni.  
Materiale didattico disponibile all''indirizzo http://www.dsi.unifi.it/user/chisci/. 
Altro materiale: 
S. Bittanti: Teoria della Predizione e del Filtraggio, Pitagora Ed., Bologna, 1993. 
B.D.O. Anderson, J.B. Moore: Optimal Filtering, Prentice Hall, 1979. 
T. Sodestrom: Discrete-Time Stochastic Systems Estimation and Control, Prentice Hall, 1994.  
Y. Bar-Shalom, X. R. Li, T. Kirubarajan: Estimation with Applications to Tracking and Navigation – Theory, Algorithms and Software, J. Wiley & Sons, 2001. 
B. Ristic, S. Arulampalam, N. Gordon: Beyond the Kalman Filter – Particle Filters for tracking Applications, Artech House, 2004.  
S. Bittanti: Identificazione dei Modelli e Controllo Adattativo, Pitagora Ed., Bologna, 1997. 
L. Ljung: System Identification - Theory for the User, Prentice Hall, 1999. 
T. Sodestrom, P. Stoica: System Identification, Prentice Hall, 1989.', 'Fornire agli studenti strumenti statistici per l''analisi e  
l''elaborazione dei dati con particolare riferimento a problemi di stima  
di parametri, segnali e modelli di sistemi dinamici. 
Utilizzo di tecniche di analisi ed elaborazione dei dati per la  
stima di parametri, segnali e modelli di sistemi dinamici.', 'Lezioni ed esercitazioni in aula.', 'L''esame consiste di un elaborato e di una prova orale.
L''elaborato, assegnato a gruppi di 1-4 studenti, è finalizzato a verificare le capacità di utilizzare strumenti informatici di elaborazione dei dati per risolvere problemi pratici relativi a  stima di parametri, segnali e modelli di sistemi dinamici.
La prova orale ha lo scopo di accertare, mediante esercizi e domande teoriche, le conoscenze acquisite su: strumenti statistici per l''analisi e  
l''elaborazione dei dati con particolare riferimento a problemi di stima  
di parametri, segnali e modelli di sistemi dinamici.', '1. SEGNALI E SISTEMI DINAMICI STOCASTICI
Processi stocastici. Momenti di un processo stocastico. Analisi in frequenza di processi stocastici. Risposta dei sistemi dinamici ad ingressi stocastici: analisi a regime. Fattorizzazione spettrale. Processi AR, MA e ARMA. Modellizzazione di processi stocastici non stazionari (processi di Wiener e ARIMA). Statistiche del primo e del secondo ordine (correlogrammi e periodogrammi). Risposta dei sistemi dinamici ad ingressi stocastici: analisi in transitorio.
2. ELEMENTI DI TEORIA DELLA STIMA
Stima puntuale in un contesto Bayesiano. Stima a minimo errore quadratico medio. Stima ottima lineare. Stima di segnali stazionari tramite il filtro  di Wiener. Predizione a minimo errore quadratico medio. Filtro di Wiener causale. 
3. FILTRAGGIO RICORSIVO
Stima dello stato di un sistema dinamico lineare.  Il filtro di Kalman come osservatore ottimo (a minimo errore quadratico medio).  Il filtro di Kalman stazionario. Dualità con il problema del regolatore ottimo LQ. Predizione e regolarizzazione. Applicazioni del filtro di Kalman. Filtraggio nonlineare.
4. IDENTIFICAZIONE DEI MODELLI
La procedura di identificazione e le sue fasi. Classificazione dei modelli.
Identificazione non parametrica: analisi di correlazione ed analisi spettrale.
Identificazione parametrica: classi di modelli; identificabilita'' strutturale; criterio di adeguatezza del minimo errore di predizione; progetto dell''esperimento ed identificabilita'' sperimentale; identificazione ad anello chiuso; scelta della struttura e validazione.', 'https://www.ing-inm.unifi.it/p-ins2-2018-502776-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (37, '2018/2019', 1, 'OPTIMIZATION METHODS', 'B024333', 'Ingegneria dell''Informazione', 'MAT/09 - RICERCA OPERATIVA', 'Primo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6', '*MANCANTE*', 'Condizioni di ottimalità
Metodi per l''ottimizzazione locale non vincolata
Metodi per l''ottimizzazione locale vincolata
Metodi di ottimizzazione per problemi di apprendimento automatico', 'Metodi di ottimizzazione non vincolata, L. Grippo, M. Sciandrone, Springer-Verlag, 2011
Dispense
Video streaming on line delle lezioni', 'Comprendere e saper utilizzare le condizioni di ottimalità; conoscere i principali approcci algoritmici per l''ottimizzazione locale e le loro proprietà teoriche e computazionali', 'Lezioni frontali. Le lezioni vengono registrate (audio e video) e rese disponibilit tramite moodle e YouTube', 'Esame scritto o orale (in alternativa) su tutto il programma.
Nel corso dell''esame  si verifica mediante quesiti e domande teoriche:
- la conoscenza della teoria dell''ottimizzazione (condizioni di ottimalità)
- la conoscenza di applicazioni dell''ottimizzazione al machine learning
- la conoscenza di algritmi di ottimizzazione non lineare', 'Introduzione: modelli di ottimizzazione, esempi
Nozioni di base e definizioni. 
Condizioni di ottimalità di KKT
Introduzione ai problemi di machine learning
Convergenza degli algoritmi
Ottimizzazione mono-dimensionale
Metodi di discesa al gradiente
Metodi di Newton
Metodi alle direzioni coniugate
Metodi Quasi-Newton
Metodi Trust region
Metodi per problemi vincolati
Algoritmi di Ottimizzazione Globale', 'https://www.ing-inm.unifi.it/p-ins2-2018-502771-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (38, '2018/2019', 1, 'OPTIMIZATION OF COMPLEX SYSTEMS', 'B024334', 'Ingegneria dell''Informazione', 'MAT/09 - RICERCA OPERATIVA', 'Primo Anno - Secondo Semestre', 'dal 25/02/2019 al 07/06/2019', '6', '*MANCANTE*', 'Metodi di decomposizione. Ottimizzazione multiobiettivo. Giochi ed equilibri. Ottimizzazione sparsa.Ot.timizzazione multiobiettivo', 'Dispense.', '1) Capacità di formulare problemi di ottimizzazione non lineare.
2) Conoscenza di algoritmi per problemi di ottimizzazione a larga scala, di ottimizzazione sparsa, di equilibrio di reti, di apprendimento automatico, di ottimizzazione multi-agente, di ottimizzazione multiobiettivo.
3) Capacità di applicare e adattare, a specifici contesti applicativi,  algoritmi standard di ottimizzazione non lineare.', 'Lezioni frontali', 'L''esame consta di una prova scritta (o alternativamente di una prova orale) nella quale si verifica mediante quesiti e domande teoriche:
- la capacità di formulare problemi di ottimizzazione;
- la capacità di utilizzare e adattare algoritmi standard di ottimizzazione; 
- la conoscenza di algoritmi per la soluzione di specifiche classi di problemi complessi di ottimizzazione.', 'Metodi di decomposizione per ottimizzazione non vincolata e ottimizzazione vincolata. Metodi di decomposizione per l''apprendimento automatico. Metodi della discesa più ripida per problemi di ottimizzzazione multiobiettivo. Giochi ed equilibri di Nash. Algoritmi esatti per l''ottimizzazione globale. Euristiche per problemi di ottimizzazione globale. Metodi di tipo LASSO per l''ottimizzazione sparsa. Metodi di programmazione concava per la minimizzazione in norma zero.
Algoritmi per ottimizzazione multiobiettivo.', 'https://www.ing-inm.unifi.it/p-ins2-2018-502770-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (39, '2018/2019', 1, 'PARALLEL COMPUTING', 'B024314/B024313', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Primo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6/9', '*MANCANTE*', 'Tecniche e strumenti di programmazione parallela', '- Principles of Parallel Programming, Calvin Lyn and Lawrence Snyder, Pearson
- Parallel Programming for Multicore and Cluster Systems, Thomas Dauber and Gudula Rünger, Springer
- Programming Massively Parallel Processors, David B. Kirk and Wen-mei W. Hwu, Morgan Kaufmann
- An introduction to Parallel Programming, Peter Pacheco, Morgan Kaufmann', 'Scopo del corso è introdurre gli studenti alle tecniche di programmazione parallela e ad alta performance.
Al termine del corso lo studente possiede le basi fondamentali di programmazione parallela per sistemi multicore, cluster e GPGPU, conosce inoltre i principali paradigmi di programmazione parallela e gli ambienti di programmazione standard Java, C++11, Pthreads, OpenMP, MPI e CUDA.', 'Lezioni frontali (80%) e attività di laboratorio (20%)', '- Progetto di sviluppo software durante il corso (30% del voto finale)
- Esame scritto intermedio (10% del voto finale)
- Progetto di sviluppo software finale (60% del voto finale)
Per ogni progetto deve essere scritta una relazione tecnica ed una presentazione che descrive il lavoro e riporta la performance rispetto ad una versione sequenziale del progetto.
Gli elaborati sono scelti dagli studenti da una lista proposta dal docente. Possono essere svolti singolarmente o in coppia.
L''elaborato è mirato a dimostrare le capacità di:
- Saper implementare un software parallelo usando uno (corso a 6 crediti) o due (corso a 9 crediti) dei framework e strumenti visti a lezione
- Saper valutare gli effetti e differenze della programmazione parallela rispetto a quella sequenziale
- Saper misurare la performance di un programma parallelo rispetto ad uno sequenziale
- Saper scrivere una relazione tecnica ed effettuare una presentazione tecnica.
L''esame scritto intermedio ha lo scopo di valutare le conoscenze relative alle tecniche di programmazione multi-core e GPU.', 'Livelli di parallelismo (istruzioni, transazioni, task, thread, memoria.)
Modelli di parallelismo (SIMD, MIMD, SPMD)
CPU e architetture parallele
Design Pattern per Programmazione concorrente (Master/Worker; Message passing)
Parallelization strategies, task parallelism, data parallelism, e work sharing
Programmazione parallela in C/C++ (C++11)/Java
Strutture dati concorrenti
Multi-core processor programming
Shared memory parallelism; OpenMP
Multithreading
Distributed network programming
Distributed memory model; MPI
Hadoop; Apache Storm e Lambda architecture
Overview GPGPU, Hardware GPU
CUDA; compilatore CUDA e strumenti
La memoria nella GPU e suo accesso
Stream e multi-GPU
Utilizzo librerie CUDA', 'https://www.ing-inm.unifi.it/p-ins2-2018-502769-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (40, '2018/2019', 1, 'SECURITY AND KNOWLEDGE MANAGEMENT', 'B028461/B028460', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Primo Anno - Primo Semestre', 'dal 24/09/2018 al 21/12/2018', '6/9', '*MANCANTE*', '-- semantic web
-- ontology engineering
-- reasoning
-- query SPARQL
-- natural language processing, NLP
-- web and mobile security
-- Secure user profiling, GDPR
-- IOT security', 'several', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '-- semantic web
-- ontology engineering
-- reasoning
-- query SPARQL
-- natural language processing, NLP
-- web and mobile security
-- Secure user profiling, GDPR
-- IOT security', 'https://www.ing-inm.unifi.it/p-ins2-2018-502772-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (41, '2018/2019', 1, 'BIG DATA ARCHITECTURES', 'B028463/B028462', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - ', 'dal  al ', '6/9', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502799-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (42, '2018/2019', 1, 'COMPUTATIONAL VISION', 'B024316/B024315', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - ', 'dal  al ', '6/9', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502795-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (43, '2018/2019', 1, 'HUMAN COMPUTER INTERACTION', 'B024324/B024323', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - ', 'dal  al ', '6/9', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502798-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (44, '2018/2019', 1, 'IMAGE PROCESSING AND SECURITY', 'B024336', 'Ingegneria dell''Informazione', 'ING-INF/03 - TELECOMUNICAZIONI', 'Secondo Anno - ', 'dal  al ', '6', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502791-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (45, '2018/2019', 1, 'LABORATORIO/TIROCINIO', 'B010516', 'Ingegneria dell''Informazione', 'NN - ', 'Secondo Anno - ', 'dal  al ', '3', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502796-0.html');
INSERT INTO public.app_course (id, academic_year, cdl_id, title, code, department, sector, teaching_schedule, teaching_period, cfu, mutuazioni, content, textbooks, objectives, methods, exam, program, link) VALUES (46, '2018/2019', 1, 'MACHINE LEARNING', 'B024318/B024317', 'Ingegneria dell''Informazione', 'ING-INF/05 - SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI', 'Secondo Anno - ', 'dal  al ', '6/9', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', '*MANCANTE*', 'https://www.ing-inm.unifi.it/p-ins2-2018-502793-0.html');


--
-- Data for Name: app_instructor; Type: TABLE DATA; Schema: public; Owner: leonardo
--

INSERT INTO public.app_instructor (id, name) VALUES (1, 'CONTI COSTANZA');
INSERT INTO public.app_instructor (id, name) VALUES (2, 'BETTINI LORENZO');
INSERT INTO public.app_instructor (id, name) VALUES (3, 'BERRETTI STEFANO');
INSERT INTO public.app_instructor (id, name) VALUES (4, 'MARINAI SIMONE');
INSERT INTO public.app_instructor (id, name) VALUES (5, 'BOREALE MICHELE');
INSERT INTO public.app_instructor (id, name) VALUES (6, 'GORI ALESSANDRO');
INSERT INTO public.app_instructor (id, name) VALUES (7, 'MARTELLI CRISTINA');
INSERT INTO public.app_instructor (id, name) VALUES (8, 'SEIDENARI LORENZO');
INSERT INTO public.app_instructor (id, name) VALUES (9, 'PALA PIETRO');
INSERT INTO public.app_instructor (id, name) VALUES (10, 'MARABISSI DANIA');
INSERT INTO public.app_instructor (id, name) VALUES (11, 'SCHOEN FABIO');
INSERT INTO public.app_instructor (id, name) VALUES (12, 'SCIANDRONE MARCO');
INSERT INTO public.app_instructor (id, name) VALUES (13, 'BERTINI MARCO');
INSERT INTO public.app_instructor (id, name) VALUES (14, 'COLOMBO CARLO');
INSERT INTO public.app_instructor (id, name) VALUES (15, 'BAGDANOV ANDREW DAVID');
INSERT INTO public.app_instructor (id, name) VALUES (16, 'PIVA ALESSANDRO');
INSERT INTO public.app_instructor (id, name) VALUES (17, 'NESI PAOLO');
INSERT INTO public.app_instructor (id, name) VALUES (18, 'FRASCONI PAOLO');
INSERT INTO public.app_instructor (id, name) VALUES (19, 'CHITI FRANCESCO');
INSERT INTO public.app_instructor (id, name) VALUES (20, 'PECORELLA TOMMASO');
INSERT INTO public.app_instructor (id, name) VALUES (21, 'VICARIO ENRICO');
INSERT INTO public.app_instructor (id, name) VALUES (22, 'FANTECHI ALESSANDRO');
INSERT INTO public.app_instructor (id, name) VALUES (23, 'FANTACCI ROMANO');
INSERT INTO public.app_instructor (id, name) VALUES (24, 'DEL BIMBO ALBERTO');
INSERT INTO public.app_instructor (id, name) VALUES (25, 'CRESCENZI PIERLUIGI');
INSERT INTO public.app_instructor (id, name) VALUES (26, 'TESI ALBERTO');
INSERT INTO public.app_instructor (id, name) VALUES (27, 'INNOCENTI GIACOMO');
INSERT INTO public.app_instructor (id, name) VALUES (28, 'BASSO MICHELE');
INSERT INTO public.app_instructor (id, name) VALUES (29, 'CHISCI LUIGI');
INSERT INTO public.app_instructor (id, name) VALUES (30, 'BELLINI PIERFRANCESCO');


--
-- Data for Name: app_course_instructors; Type: TABLE DATA; Schema: public; Owner: leonardo
--

INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (1, 1, 1);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (2, 2, 2);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (3, 3, 3);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (4, 4, 4);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (5, 5, 5);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (6, 6, 6);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (7, 7, 6);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (8, 8, 7);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (9, 9, 7);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (10, 10, 8);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (11, 11, 9);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (12, 12, 10);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (13, 13, 11);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (14, 14, 12);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (15, 15, 13);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (16, 16, 14);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (17, 17, 15);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (18, 18, 17);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (19, 19, 48);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (20, 20, 50);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (21, 21, 51);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (22, 22, 22);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (23, 23, 23);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (24, 24, 24);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (25, 25, 25);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (26, 26, 27);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (27, 27, 33);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (28, 28, 35);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (29, 29, 36);
INSERT INTO public.app_course_instructors (id, instructor_id, course_id) VALUES (30, 30, 40);


--
-- Name: app_cdl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: leonardo
--

SELECT pg_catalog.setval('public.app_cdl_id_seq', 1, true);


--
-- Name: app_course_id_seq; Type: SEQUENCE SET; Schema: public; Owner: leonardo
--

SELECT pg_catalog.setval('public.app_course_id_seq', 54, true);


--
-- Name: app_course_instructors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: leonardo
--

SELECT pg_catalog.setval('public.app_course_instructors_id_seq', 30, true);


--
-- Name: app_instructor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: leonardo
--

SELECT pg_catalog.setval('public.app_instructor_id_seq', 30, true);


--
-- PostgreSQL database dump complete
--

